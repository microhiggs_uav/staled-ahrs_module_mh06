
#include <stdio.h>
#include <string.h>

#include <math.h>

#include "math_cal.h"
#include "icm20602.h"
#include "IST8310.h"
#include "system.h"
#include "comm_link.h"
#include "att_est.h"

#define M_PI_F						3.141592653f
#define	CONSTANTS_ONE_G		9.81f

float quat_ahrs[4];
float gyro_bias[3];
float rate_ahrs[3];

void quat_product(float p[4],float q[4],float r[4]);
void quat_inverse(float p[4],float r[4]);
void rotate_vec_quat(float vec_in[3],float q[4],float vec_out[3]);

void quaternion_conjugate(float v[3], float q[4], float output[3]);
void quaternion_conjugate_inversed(float v[3], float q[4], float output[3]);

void quater_to_dcm(float q[4],float dcm[3][3]);
void euler_from_dcm(float dcm[3][3],float euler[3]);

float wrap_pi(float x);
float wrap_2pi(float angle);
float constrain_float(float data, float min, float max);
void vector_3_norm(float vec[3]);
void quaternion_dcm(float R[3][3], float q[4]);

float gyro_integral_init[3] = {0.0f};
bool inited_ahrs = false;
uint32_t counter_init = 0;
bool init_AHRS(float gyro[3],float acc[3],float mag[3])
{
	if(inited_ahrs) return true;
	
	counter_init++;
	if(counter_init<30) return false;
	
	memset(quat_ahrs,0x00,sizeof(quat_ahrs));
	quat_ahrs[0] = 1.0f;
	memset(gyro_bias,0x00,sizeof(gyro_bias));
	memset(rate_ahrs,0x00,sizeof(rate_ahrs));
	
	float k_init[3];
	k_init[0] = -acc[0];
	k_init[1] = -acc[1];
	k_init[2] = -acc[2];
	float norm_k;
	norm_k = sqrtf(k_init[0]*k_init[0]+k_init[1]*k_init[1]+k_init[2]*k_init[2]);
	if (norm_k < 0.1f) return 0; 
	norm_k = 1.0f/norm_k;       
	k_init[0] *= norm_k;
	k_init[1] *= norm_k;
	k_init[2] *= norm_k;
	
	float mag_k=mag[0]*k_init[0]+mag[1]*k_init[1]+mag[2]*k_init[2];
	float i_init[3];
	for (int i = 0; i < 3; i++) {
		i_init[i] = mag[i] - mag_k*k_init[i];
	}
	vector_3_norm(i_init);
	
	float j_init[3];
	j_init[0] = k_init[1]*i_init[2]-k_init[2]*i_init[1];
	j_init[1] = k_init[2]*i_init[0]-k_init[0]*i_init[2];
	j_init[2] = k_init[0]*i_init[1]-k_init[1]*i_init[0];
	
	float R_int[3][3];
	for (int i = 0; i < 3; i++) {
		R_int[0][i] = i_init[i];
		R_int[1][i] = j_init[i];
		R_int[2][i] = k_init[i];
	}

	quaternion_dcm(R_int, quat_ahrs);
	float dcm[3][3];
	quater_to_dcm(quat_ahrs,dcm);
	euler_from_dcm(dcm,gyro_integral_init);
	if(gyro_integral_init[2]<0.0f){
		gyro_integral_init[2] += 2.0f*M_PI_F;
	}
	
	inited_ahrs = true;
	return true;
}


float gyro_x_integral=0.0f;
float gyro_y_integral=0.0f;
float gyro_z_integral=0.0f;
void update_AHRS(float gyro[3],float acc[3],float mag[3],float dt)
{
	float q0q0 = quat_ahrs[0]*quat_ahrs[0];
	float q0q1 = quat_ahrs[0]*quat_ahrs[1];
	float q0q2 = quat_ahrs[0]*quat_ahrs[2];
	float q0q3 = quat_ahrs[0]*quat_ahrs[3];
	float q1q1 = quat_ahrs[1]*quat_ahrs[1];
	float q1q2 = quat_ahrs[1]*quat_ahrs[2];
	float q1q3 = quat_ahrs[1]*quat_ahrs[3];
	float q2q2 = quat_ahrs[2]*quat_ahrs[2];
	float q2q3 = quat_ahrs[2]*quat_ahrs[3];
	float q3q3 = quat_ahrs[3]*quat_ahrs[3];

	float norm_mag;
	norm_mag = sqrtf(mag[0]*mag[0]+mag[1]*mag[1]+mag[2]*mag[2]);
	if(norm_mag>0.001f){ 
		norm_mag = 1.0f/norm_mag;     
		mag[0] *= norm_mag;
		mag[1] *= norm_mag;
		mag[2] *= norm_mag;
	}
//	vector_3_norm(mag);
	
#if 1	
	float mag_earth[3];
	mag_earth[0] = (1.0f-2.0f*(q2q2+q3q3))*mag[0]
								+2.0f*(q1q2-q0q3)*mag[1]
								+2.0f*(q1q3+q0q2)*mag[2];
	mag_earth[1] = 2.0f*(q1q2+q0q3)*mag[0]
								+(1.0f-2.0f*(q1q1+q3q3))*mag[1]
								+2.0f*(q2q3-q0q1)*mag[2];
	mag_earth[2] = 2.0f*(q1q3-q0q2)*mag[0]
								+2.0f*(q2q3+q0q1)*mag[1]
								+(1.0f-2.0f*(q1q1+q2q2))*mag[2];

	float mag_decl = update_mag_declination();
//	printf("mag_decl:%6.3f\r\n",mag_decl);
	float mag_err = atan2f(mag_earth[1],mag_earth[0]) - mag_decl;
	mag_err = wrap_pi(mag_err);
	
	float gainMult = 1.0f;
	const float fifty_dps = 0.873f;

	float mag_err_body[3];
	mag_err_body[0] = -2.0f*(q1q3-q0q2)*mag_err;
	mag_err_body[1] = -2.0f*(q2q3+q0q1)*mag_err;
	mag_err_body[2] = -(1.0f-2.0f*(q1q1+q2q2))*mag_err;
#endif

#if 0
	float mag_earth[3];
	quaternion_conjugate(mag, quat_ahrs, mag_earth);
	float mag_err = atan2f(mag_earth[1],mag_earth[0]);
	mag_err = wrap_pi(mag_err);

	float gainMult = 1.0f;
	const float fifty_dps = 0.873f;

	float mag_err_vec[3]={0.0f,0.0f,-mag_err};
	float mag_err_body[3];
	quaternion_conjugate_inversed(mag_err_vec, quat_ahrs, mag_err_body);
#endif

	float spinRate = sqrtf(gyro[0]*gyro[0]+gyro[1]*gyro[1]+gyro[2]*gyro[2]);
	if (spinRate > fifty_dps) {
		gainMult = spinRate / fifty_dps;
		if(gainMult>10.0f)	gainMult=10.0f;
	}
//	printf("gainMult:%6.3f\r\n",gainMult);
	const float _w_mag = 1.0f;		// 0.1f;
	float corr[3];
	corr[0] =  _w_mag * gainMult * mag_err_body[0];
	corr[1] =  _w_mag * gainMult * mag_err_body[1];
	corr[2] =  _w_mag * gainMult * mag_err_body[2];
	
	float norm_qq = (float)sqrtf(quat_ahrs[0]*quat_ahrs[0]+quat_ahrs[1]*quat_ahrs[1]+quat_ahrs[2]*quat_ahrs[2]+quat_ahrs[3]*quat_ahrs[3]);
	norm_qq = 1.0f / norm_qq;
	quat_ahrs[0] = quat_ahrs[0] * norm_qq;
	quat_ahrs[1] = quat_ahrs[1] * norm_qq;
	quat_ahrs[2] = quat_ahrs[2] * norm_qq;
	quat_ahrs[3] = quat_ahrs[3] * norm_qq;

	float k_ahrs[3];
	k_ahrs[0]	=	2.0f*(q1q3-q0q2);
	k_ahrs[1]	=	2.0f*(q2q3+q0q1),
	k_ahrs[2]	=	q0q0-q1q1-q2q2+q3q3;
	
	float acc_err[3] = {0.0f};
	float norm_acc = sqrtf(acc[0]*acc[0]+acc[1]*acc[1]+acc[2]*acc[2]);
	const float lower_accel_limit = CONSTANTS_ONE_G * 0.9f;
	const float upper_accel_limit = CONSTANTS_ONE_G * 1.1f;
	if (norm_acc > lower_accel_limit && norm_acc < upper_accel_limit ){
		vector_3_norm(acc);
		acc_err[0] = k_ahrs[1]*acc[2]-k_ahrs[2]*acc[1];
		acc_err[1] = k_ahrs[2]*acc[0]-k_ahrs[0]*acc[2];
		acc_err[2] = k_ahrs[0]*acc[1]-k_ahrs[1]*acc[0];		
	}
	
	const float _w_acc = 0.2f;
	corr[0] +=  _w_acc * acc_err[0];
	corr[1] +=  _w_acc * acc_err[1];
	corr[2] +=  _w_acc * acc_err[2];
	
	// Gyro bias estimation
	const float _w_gyro_bias = 0.1f;
	const float _bias_max = 0.05f;
	if (spinRate < 0.175f) {
		for (int i = 0; i < 3; i++) {
			gyro_bias[i] += corr[i] * (_w_gyro_bias * dt);
			gyro_bias[i] = constrain_float(gyro_bias[i], -_bias_max, _bias_max);
		}
	}
	
	rate_ahrs[0] = gyro[0] + gyro_bias[0];
	rate_ahrs[1] = gyro[1] + gyro_bias[1];
	rate_ahrs[2] = gyro[2] + gyro_bias[2];
	
	corr[0] = 2.0f*corr[0] + rate_ahrs[0];
	corr[1] = 2.0f*corr[1] + rate_ahrs[1];
	corr[2] = 2.0f*corr[2] + rate_ahrs[2];
	
	float p0 = quat_ahrs[0];
	float p1 = quat_ahrs[1];
	float p2 = quat_ahrs[2];
	float p3 = quat_ahrs[3];
	
	quat_ahrs[0] = quat_ahrs[0] + (-p1 * corr[0] - p2 * corr[1] - p3 * corr[2]) * (0.5f * dt);
	quat_ahrs[1] = quat_ahrs[1] + (p0 * corr[0] + p2 * corr[2] - p3 * corr[1]) * (0.5f * dt);
	quat_ahrs[2] = quat_ahrs[2] + (p0 * corr[1] - p1 * corr[2] + p3 * corr[0]) * (0.5f * dt);
	quat_ahrs[3] = quat_ahrs[3] + (p0 * corr[2] + p1 * corr[1] - p2 * corr[0]) * (0.5f * dt);

	// Normalise quaternion
	float norm_q = (float)sqrtf(quat_ahrs[0]*quat_ahrs[0]+quat_ahrs[1]*quat_ahrs[1]+quat_ahrs[2]*quat_ahrs[2]+quat_ahrs[3]*quat_ahrs[3]);
	norm_q = 1.0f / norm_q;
	quat_ahrs[0] = quat_ahrs[0] * norm_q;
	quat_ahrs[1] = quat_ahrs[1] * norm_q;
	quat_ahrs[2] = quat_ahrs[2] * norm_q;
	quat_ahrs[3] = quat_ahrs[3] * norm_q;
	
	// for mag calibration
	gyro_x_integral += gyro[0] * dt;
	gyro_y_integral += gyro[1] * dt;
	gyro_z_integral += gyro[2] * dt;
	gyro_x_integral = wrap_pi(gyro_x_integral);
	gyro_y_integral = wrap_pi(gyro_y_integral);
	gyro_z_integral = wrap_pi(gyro_z_integral);
}

void get_angle_rpy(float angle_rpy[3])
{
#if 0
	angle_rpy[0] = 
	atan2f(2.0f*(quat_ahrs[2]*quat_ahrs[3]+quat_ahrs[0]*quat_ahrs[1] ), 
	quat_ahrs[0]*quat_ahrs[0]-quat_ahrs[1]*quat_ahrs[1]-quat_ahrs[2]*quat_ahrs[2]+quat_ahrs[3]*quat_ahrs[3]);

	angle_rpy[1] = -asinf(2.0f*(quat_ahrs[1]*quat_ahrs[3]-quat_ahrs[0]*quat_ahrs[2]));

	angle_rpy[2] = atan2f(2.0f*(quat_ahrs[1]*quat_ahrs[2]+quat_ahrs[0]*quat_ahrs[3] ) , 
	quat_ahrs[0]*quat_ahrs[0]+quat_ahrs[1]*quat_ahrs[1]-quat_ahrs[2]*quat_ahrs[2]-quat_ahrs[3]*quat_ahrs[3]);
#endif
	
	float dcm[3][3];
	quater_to_dcm(quat_ahrs,dcm);
	euler_from_dcm(dcm,angle_rpy);
	if(angle_rpy[2]<0.0f){
		angle_rpy[2] += 2.0f*M_PI_F;
	}
}

void get_gyro_integral(float gyro_integ[3])
{
	gyro_integ[0] = gyro_integral_init[0]+gyro_x_integral;
	gyro_integ[1] = gyro_integral_init[1]+gyro_y_integral;
	gyro_integ[2] = gyro_integral_init[2]+gyro_z_integral;
}

// q*p:=Q(q)p
// rotate p by q
void quat_product(float p[4],float q[4],float r[4])
{
	r[0] = p[0] * q[0] - p[1] * q[1] - p[2] * q[2] - p[3] * q[3];
	r[1] = p[0] * q[1] + p[1] * q[0] + p[2] * q[3] - p[3] * q[2];
	r[2] = p[0] * q[2] - p[1] * q[3] + p[2] * q[0] + p[3] * q[1];
	r[3] = p[0] * q[3] + p[1] * q[2] - p[2] * q[1] + p[3] * q[0];
}

void quat_inverse(float p[4],float r[4])
{
	float normSq = p[0]*p[0]+p[1]*p[1]+p[2]*p[2]+p[3]*p[3];
	if(normSq>0.00001f){
		r[0] = p[0]/normSq;
		r[1] = -p[1]/normSq;
		r[2] = -p[2]/normSq;
		r[3] = -p[3]/normSq;
	}else{
		r[0] = 0.0f;
		r[1] = 0.0f;
		r[2] = 0.0f;
		r[3] = 0.0f;
	}
}
 
void rotate_vec_quat(float vec_in[3],float q[4],float vec_out[3])
{
	float q0q0 = q[0]*q[0];
	float q0q1 = q[0]*q[1];
	float q0q2 = q[0]*q[2];
	float q0q3 = q[0]*q[3];
	float q1q1 = q[1]*q[1];
	float q1q2 = q[1]*q[2];
	float q1q3 = q[1]*q[3];
	float q2q2 = q[2]*q[2];
	float q2q3 = q[2]*q[3];
	float q3q3 = q[3]*q[3];
	vec_out[0] = (1.0f-2.0f*q2q2-2.0f*q3q3)*vec_in[0]
				+(2.0f*q1q2-2.0f*q0q3)*vec_in[1]
				+(2.0f*q1q3+2.0f*q0q2)*vec_in[2];
	vec_out[1] = (2.0f*q1q2+2.0f*q0q3)*vec_in[0]
				+(1.0f-2.0f*q1q1-2.0f*q3q3)*vec_in[1]
				+(2.0f*q2q3-2.0f*q0q1)*vec_in[2];
	vec_out[2] = (2.0f*q1q3-2.0f*q0q2)*vec_in[0]
				+(2.0f*q2q3+2.0f*q0q1)*vec_in[1]
				+(1.0f-2.0f*q1q1-2.0f*q2q2)*vec_in[2];
}


float wrap_pi(float angle)
{
		if (angle > (3.0f * M_PI_F) || angle < (-3.0f * M_PI_F)) {
        // for large numbers use modulus
        angle = fmod(angle,(2.0f * M_PI_F));
    }
    if (angle > M_PI_F) { angle -= (2.0f * M_PI_F); }
    if (angle < -M_PI_F) { angle += (2.0f * M_PI_F); }
    return angle;
}

float wrap_2pi(float angle)
{
		if (angle > (4.0f * M_PI_F) || angle < (-2.0f * M_PI_F)) {
        // for large numbers use modulus
        angle = fmod(angle,(2.0f * M_PI_F));
    }
			
		if(angle > (2.0f * M_PI_F)) {
        // for large numbers use modulus
        angle -= 2.0f * M_PI_F;
    }else if(angle < 0.0f){
				angle += 2.0f * M_PI_F;
		}
    return angle;
}


void quaternion_dcm(float R[3][3], float q[4])
{
		float t = R[0][0]*R[0][0]+R[1][1]*R[1][1]+R[2][2]*R[2][2];
		if (t > 0.0f) {
				t = sqrtf(1.0f + t);
				q[0] = 0.5f * t;
				t = 0.5f / t;
				q[1] = (R[2][1] - R[1][2]) * t;
				q[2] = (R[0][2] - R[2][0]) * t;
				q[3] = (R[1][0] - R[0][1]) * t;
		} else if (R[0][0] > R[1][1] && R[0][0] > R[2][2]) {
				t = sqrtf(1.0f + R[0][0] - R[1][1] - R[2][2]);
				q[1] = 0.5f * t;
				t = 0.5f / t;
				q[0] = (R[2][1] - R[1][2]) * t;
				q[2] = (R[1][0] + R[0][1]) * t;
				q[3] = (R[0][2] + R[2][0]) * t;
		} else if (R[1][1] > R[2][2]) {
				t = sqrtf(1.0f - R[0][0] + R[1][1] - R[2][2]);
				q[2] = 0.5f * t;
				t = 0.5f / t;
				q[0] = (R[0][2] - R[2][0]) * t;
				q[1] = (R[1][0] + R[0][1]) * t;
				q[3] = (R[2][1] + R[1][2]) * t;
		} else {
				t = sqrtf(1.0f - R[0][0] - R[1][1] + R[2][2]);
				q[3] = 0.5f * t;
				t = 0.5f / t;
				q[0] = (R[1][0] - R[0][1]) * t;
				q[1] = (R[0][2] + R[2][0]) * t;
				q[2] = (R[2][1] + R[1][2]) * t;
		}
}


// frame rotation by q
//void quaternion_conjugate(float v[3], float q[4], float output[3])
void quaternion_conjugate_inversed(float v[3], float q[4], float output[3])
{
    float result[3];

    float ww = q[0] * q[0];
    float xx = q[1] * q[1];
    float yy = q[2] * q[2];
    float zz = q[3] * q[3];
    float wx = q[0] * q[1];
    float wy = q[0] * q[2];
    float wz = q[0] * q[3];
    float xy = q[1] * q[2];
    float xz = q[1] * q[3];
    float yz = q[2] * q[3];

    // Formula from http://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/transforms/index.htm
    // p2.x = w*w*p1.x + 2*y*w*p1.z - 2*z*w*p1.y + x*x*p1.x + 2*y*x*p1.y + 2*z*x*p1.z - z*z*p1.x - y*y*p1.x;
    // p2.y = 2*x*y*p1.x + y*y*p1.y + 2*z*y*p1.z + 2*w*z*p1.x - z*z*p1.y + w*w*p1.y - 2*x*w*p1.z - x*x*p1.y;
    // p2.z = 2*x*z*p1.x + 2*y*z*p1.y + z*z*p1.z - 2*w*y*p1.x - y*y*p1.z + 2*w*x*p1.y - x*x*p1.z + w*w*p1.z;

    result[0] = ww*v[0] - 2*wy*v[2] + 2*wz*v[1] +
                xx*v[0] + 2*xy*v[1] + 2*xz*v[2] -
                zz*v[0] - yy*v[0];
    result[1] = 2*xy*v[0] + yy*v[1] + 2*yz*v[2] -
                2*wz*v[0] - zz*v[1] + ww*v[1] +
                2*wx*v[2] - xx*v[1];
    result[2] = 2*xz*v[0] + 2*yz*v[1] + zz*v[2] +
                2*wy*v[0] - yy*v[2] - 2*wx*v[1] -
                xx*v[2] + ww*v[2];

    // Copy result to output
    output[0] = result[0];
    output[1] = result[1];
    output[2] = result[2];
}


// vector rotation by q
//void quaternion_conjugate_inversed(float v[3], float q[4], float output[3])
void quaternion_conjugate(float v[3], float q[4], float output[3])
{
    float result[3];

    float ww = q[0] * q[0];
    float xx = q[1] * q[1];
    float yy = q[2] * q[2];
    float zz = q[3] * q[3];
    float wx = q[0] * q[1];
    float wy = q[0] * q[2];
    float wz = q[0] * q[3];
    float xy = q[1] * q[2];
    float xz = q[1] * q[3];
    float yz = q[2] * q[3];

    // Formula from http://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/transforms/index.htm
    // p2.x = w*w*p1.x + 2*y*w*p1.z - 2*z*w*p1.y + x*x*p1.x + 2*y*x*p1.y + 2*z*x*p1.z - z*z*p1.x - y*y*p1.x;
    // p2.y = 2*x*y*p1.x + y*y*p1.y + 2*z*y*p1.z + 2*w*z*p1.x - z*z*p1.y + w*w*p1.y - 2*x*w*p1.z - x*x*p1.y;
    // p2.z = 2*x*z*p1.x + 2*y*z*p1.y + z*z*p1.z - 2*w*y*p1.x - y*y*p1.z + 2*w*x*p1.y - x*x*p1.z + w*w*p1.z;

    result[0] = ww*v[0] + 2*wy*v[2] - 2*wz*v[1] +
                xx*v[0] + 2*xy*v[1] + 2*xz*v[2] -
                zz*v[0] - yy*v[0];
    result[1] = 2*xy*v[0] + yy*v[1] + 2*yz*v[2] +
                2*wz*v[0] - zz*v[1] + ww*v[1] -
                2*wx*v[2] - xx*v[1];
    result[2] = 2*xz*v[0] + 2*yz*v[1] + zz*v[2] -
                2*wy*v[0] - yy*v[2] + 2*wx*v[1] -
                xx*v[2] + ww*v[2];

    // Copy result to output
    output[0] = result[0];
    output[1] = result[1];
    output[2] = result[2];
}

#if 1
void quater_to_dcm(float q[4],float dcm[3][3])
{
		float a = q[0];
		float b = q[1];
		float c = q[2];
		float d = q[3];
		float aa = a * a;
		float ab = a * b;
		float ac = a * c;
		float ad = a * d;
		float bb = b * b;
		float bc = b * c;
		float bd = b * d;
		float cc = c * c;
		float cd = c * d;
		float dd = d * d;
		dcm[0][0] = aa + bb - cc - dd;
		dcm[0][1] = 2.0f * (bc - ad);
		dcm[0][2] = 2.0f * (ac + bd);
		dcm[1][0] = 2.0f * (bc + ad);
		dcm[1][1] = aa - bb + cc - dd;
		dcm[1][2] = 2.0f * (cd - ab);
		dcm[2][0] = 2.0f * (bd - ac);
		dcm[2][1] = 2.0f * (ab + cd);
		dcm[2][2] = aa - bb - cc + dd;
}

void euler_from_dcm(float dcm[3][3],float euler[3])
{
		float phi_val = atan2f(dcm[2][1], dcm[2][2]);
		float theta_val = asinf(-dcm[2][0]);
		float psi_val = atan2f(dcm[1][0], dcm[0][0]);
		float pi = M_PI_F;

		if (fabsf(theta_val - 0.5f*pi) < 0.01f) {
				phi_val = 0.0f;
				psi_val = atan2f(dcm[1][2], dcm[0][2]);

		} else if (fabsf(theta_val + 0.5f*pi) < 0.01f) {
				phi_val = 0.0f;
				psi_val = atan2f(-dcm[1][2], -dcm[0][2]);
		}

		euler[0] = phi_val;
		euler[1] = theta_val;
		euler[2] = psi_val;
}
		
#endif

