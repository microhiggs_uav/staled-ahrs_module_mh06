

#ifndef _COMM_LINK_H_
#define _COMM_LINK_H_

#include <stdio.h>
#include <string.h>
#include "system.h"
#include "icm20602.h"
#include "IST8310.h"
#include "flash.h"
#include "sensors.h"
#include "calibrator.h"

#define HEADER1_COMM_LINK		0x55
#define HEADER2_COMM_LINK		0xAA

struct comm_link_payload_s{
	enum sensors_read_status_e read_status_imu;
	enum calibrating_status_e calib_status_imu;
	enum sensors_read_status_e read_status_mag;
	enum calibrating_status_e calib_status_mag;
	enum flash_status_e flash_status;
	uint8_t reserved;
	uint8_t imu_progress;
	uint8_t mag_progress;
	int32_t gyro[3];
	int32_t acc[3];
	int32_t mag[3];
	int32_t att[3];
};

enum sensors_calib_rec_e{
    SENSORS_CALIB_DONOTHING_REC = 0,
		SENSORS_CALIB_IMU_REC,
		SENSORS_CALIB_MAG_REC,
};

struct comm_link_payload_rec_s{
	enum sensors_calib_rec_e calib_rec;
	uint8_t reserved[3];
	int32_t lat;
	int32_t lon;
};
void comm_link_init(void);
void comm_link_update(void);

void comm_link_decode(void);

float update_mag_declination();


#endif






