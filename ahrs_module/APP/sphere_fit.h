#ifndef _SPHERE_FIT_H_
#define _SPHERE_FIT_H_

int sphere_fit_least_squares(const float *samples_x,const float *samples_y,const float *samples_z,unsigned int size, unsigned int max_iterations, float delta,
	float *origin_x,float *origin_y,float *origin_z, float *sphere_radius);

#if 0
int ellipsoid_fit_least_squares(const float x[], const float y[], const float z[],
				unsigned int size, int max_iterations, float delta, float *offset_x, float *offset_y, float *offset_z,
				float *sphere_radius, float *diag_x, float *diag_y, float *diag_z, float *offdiag_x, float *offdiag_y, float *offdiag_z);
#endif

#endif

