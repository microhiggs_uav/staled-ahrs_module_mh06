
#ifndef _GEO_MAG_DECLINATION_H_
#define _GEO_MAG_DECLINATION_H_

// Return magnetic declination in degrees
float get_mag_declination(float lat, float lon);

// Return magnetic field inclination in degrees
float get_mag_inclination(float lat, float lon);

// return magnetic field strength in mTesla
float get_mag_strength(float lat, float lon);

#endif

