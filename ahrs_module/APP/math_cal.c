
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>

#include "math_cal.h"


float constrain_float(float data, float min, float max)
{
	if(data < min) return min;
	if(data > max) return max;
	return data;
}

void vector_3_norm(float vec[3])
{
	float norm_vec;
	norm_vec = sqrtf(vec[0]*vec[0]+vec[1]*vec[1]+vec[2]*vec[2]);
	if(norm_vec > 0.0000001f){
		norm_vec = 1.0f/norm_vec;        // use reciprocal for division
		vec[0] *= norm_vec;
		vec[1] *= norm_vec;
		vec[2] *= norm_vec;
	}else{
		vec[0] = 0.0f;
		vec[1] = 0.0f;
		vec[2] = 0.0f;
	}
}

