#ifndef _CALIBRATOR_H_
#define _CALIBRATOR_H_


#include <stdint.h>

enum calibrating_status_e{
		CALIB_STATUS_START = 0,
    CALIB_STATUS_DOING,
    CALIB_STATUS_CALIBED,
		CALIB_STATUS_FAILED,
};


struct calibrate_status_s{
    enum calibrating_status_e acc;
    enum calibrating_status_e gyro;
    enum calibrating_status_e mag;
    uint8_t  acc_progress;
    uint8_t  gyro_progress;
    uint8_t  mag_progress;
};

enum detect_orientation_return {
	DETECT_ORIENTATION_TAIL_DOWN = 0,
	DETECT_ORIENTATION_NOSE_DOWN,
	DETECT_ORIENTATION_LEFT,
	DETECT_ORIENTATION_RIGHT,
	DETECT_ORIENTATION_UPSIDE_DOWN,
	DETECT_ORIENTATION_RIGHTSIDE_UP,
	DETECT_ORIENTATION_ERROR
};

enum calibrate_return {
	calibrate_return_ok,
	calibrate_return_doing,
	calibrate_return_error,
	calibrate_return_cancelled
};


void imu_calib_start(void);
void mag_calib_start(void);

void calib_init(void);
void calib_update(float gyro[3],float acc[3],float mag[3]);

void imu_offset_get(float gyro[3],float acc[3]);
void mag_offset_get(float mag[3]);

void imu_calibrated_get(float gyro[3],float acc[3]);

void param_calib_load(float gyro[3],float acc[3],float mag[3]);

struct calibrate_status_s calib_status_get(void);

void imu_calib_progress_get(uint8_t progress[2]);

void mag_value_calibrated_get(uint8_t mag_cal[3]);

#endif
