

#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <stdint.h>
#include <stdbool.h>

float constrain_float(float data, float min, float max);
void vector_3_norm(float vec[3]);

#endif






