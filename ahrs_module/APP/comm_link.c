
#include <math.h>
#include "UART.h"
#include "MahonyAHRS.h"
#include "comm_link.h"
#include "geo_mag_declination.h"

#define LEN_FRAME_TX			60
uint8_t buffer_tx[LEN_FRAME_TX] = {0};
struct comm_link_payload_s palyload_tx;
struct comm_link_payload_rec_s palyload_rx;

uint16_t crc16_calc(uint8_t *data_frame, uint8_t crc16_length);

void comm_link_init()
{
	memset(&buffer_tx,0x00,sizeof(buffer_tx));
	buffer_tx[0]=HEADER1_COMM_LINK;
	buffer_tx[1]=HEADER2_COMM_LINK;
	
	memset(&palyload_tx,0x00,sizeof(palyload_tx));
	memset(&palyload_rx,0x00,sizeof(palyload_rx));
}


void comm_link_encode()
{
	struct calibrate_status_s calib=calib_status_get();
	
	palyload_tx.read_status_imu=imu_read_status_get();
	palyload_tx.calib_status_imu=calib.acc;
	palyload_tx.read_status_mag=mag_read_status_get();
	palyload_tx.calib_status_mag=calib.mag;
	palyload_tx.flash_status=FLASH_Status_get();
	palyload_tx.reserved=0x00;
	palyload_tx.imu_progress=calib.acc_progress;
	palyload_tx.mag_progress=calib.mag_progress;
	
	float gyro[3],acc[3],mag[3];
	imu_gyro_get(gyro);
	imu_acc_get(acc);
	mag_value_get(mag);
//	mag_value_calibrated_get(mag);
	
	float attitude[3];
	get_angle_rpy(attitude);
	for(int i=0;i<3;i++){
		palyload_tx.gyro[i] = 1000.0f*gyro[i];
		palyload_tx.acc[i] = 1000.0f*acc[i];
		palyload_tx.mag[i] = 1000.0f*mag[i];	
		palyload_tx.att[i] = 1000.0f*57.3f*attitude[i];
//		palyload_tx.att[i] = 1000.0f*mag[i];		// for test
	}
//	float mag_norm=sqrtf(mag[0]*mag[0]+mag[1]*mag[1]+mag[2]*mag[2]);
//	palyload_tx.att[2] = 1000.0f*mag_norm;
	memcpy(&buffer_tx[2],&palyload_tx,sizeof(palyload_tx));
	
	typedef union{
		uint8_t    b[2];
		uint16_t    w;
	}uint16_t_bytes_u;
	uint16_t_bytes_u temp;
	temp.w=crc16_calc((uint8_t *)&palyload_tx, sizeof(palyload_tx));
	buffer_tx[58]=temp.b[0];
	buffer_tx[59]=temp.b[1];
}

enum decode_step_e{
		DECODE_STEP_HEADER1 = 0,
		DECODE_STEP_HEADER2,
    DECODE_STEP_PAYLOAD,
    DECODE_STEP_CHECKSUM,
		DECODE_STEP_OTHER,
};
enum decode_step_e decode_step = DECODE_STEP_HEADER1;

uint8_t Data_rec[UART2_REC_BUFFER_LEN] = {0};	
uint8_t frame_new = 0;
void comm_link_decode()
{
	if(UART2_RX_Updata_fig==1){
		memcpy(Data_rec,Uart2_Rx_Data,UART2_REC_BUFFER_LEN);
		UART2_RX_Updata_fig=0;
	}else{
		return;
	}
	
	for(int num=0;num<UART2_REC_BUFFER_LEN;num++){
		switch(decode_step){
			case DECODE_STEP_HEADER1:
				if(HEADER2_COMM_LINK==Data_rec[num]){
					decode_step = DECODE_STEP_HEADER2;
				}
				break;
			case DECODE_STEP_HEADER2:
				if(HEADER1_COMM_LINK==Data_rec[num]){
					decode_step = DECODE_STEP_CHECKSUM;
				}else{
					decode_step = DECODE_STEP_HEADER1;
				}
				break;
			case DECODE_STEP_CHECKSUM:
			{
				uint16_t checksum_cal=crc16_calc(&Data_rec[num], 4);
				
				typedef union{
					uint8_t    b[2];
					uint16_t    w;
				}uint16_t_bytes_u;
				uint16_t_bytes_u temp;
				
				temp.b[0]=Data_rec[6];
				temp.b[1]=Data_rec[7];
				uint16_t checksum_rec = temp.w;
				if(checksum_rec==checksum_cal){
					decode_step = DECODE_STEP_PAYLOAD;
				}else{
					decode_step = DECODE_STEP_HEADER1;
				}
				break;
			}
			case DECODE_STEP_PAYLOAD:
			{
				memcpy(&palyload_rx,&Data_rec[2],sizeof(palyload_rx));
				frame_new = 1;
				decode_step = DECODE_STEP_HEADER1;
				return;
			}
			default:
				decode_step = DECODE_STEP_HEADER1;
				break;
		}
	}
}

static float lat = 22.6052728f;
static float lon = 114.4149919f;
void comm_link_update()
{
	comm_link_encode();
	Uart2_Dma_Send(buffer_tx, LEN_FRAME_TX);

	comm_link_decode();
	if(frame_new == 1){
		if(palyload_rx.calib_rec==SENSORS_CALIB_IMU_REC){
			imu_calib_start();
		}else if(palyload_rx.calib_rec==SENSORS_CALIB_MAG_REC){
			mag_calib_start();
		}
		
		lat = palyload_rx.lat*1e-7;
		lon = palyload_rx.lon*1e-7;
		printf("geo_location:%6.3f,%6.3f\r\n",lat,lon);
		frame_new = 0;
	}
	
}


uint16_t crc16_calc(uint8_t *data_frame, uint8_t crc16_length)
{
	uint16_t crc = 0xFFFF;

	for (uint8_t i = 0; i < crc16_length; i++) {
		crc ^= data_frame[i];

		for (uint8_t j = 0; j < 8; j++) {
			if (crc & 1) {
				crc = (crc >> 1) ^ 0xA001;
			} else {
				crc >>= 1;
			}
		}
	}

	return crc;
}

float update_mag_declination()
{
	return 0.01745*get_mag_declination(lat, lon);
}
