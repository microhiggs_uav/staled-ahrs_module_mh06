
#include <stdio.h>
#include <string.h>


#include <string.h>
#include <math.h>

#include "icm20602.h"
#include "IST8310.h"
#include "system.h"

#include "MahonyAHRS.h"


#define KP_AHRS_DEF  (0.4f) 
#define KI_AHRS_DEF  (0.001f) 

#define KP_MAG_AHRS_DEF  (2.0f) 
#define KI_MAG_AHRS_DEF  (0.001f) 

float quat_ahrs[4];
float eInt[3] = {0.0f};
float eInt_mag[3] = {0.0f};

static void vector_3_norm(float vec[3]);
static void quaternion_dcm(float R[3][3], float q[4]);

static float wrap_pi(float angle);

bool inited_ahrs = false;
uint32_t counter_init = 0;
bool init_AHRS(float gyro[3],float acc[3],float mag[3])
{
	if(inited_ahrs) return true;
	
	counter_init++;
	if(counter_init<30) return false;
	
	float k_init[3];
	k_init[0] = -acc[0];
	k_init[1] = -acc[1];
	k_init[2] = -acc[2];
	float norm_k;
	norm_k = sqrtf(k_init[0]*k_init[0]+k_init[1]*k_init[1]+k_init[2]*k_init[2]);
	if (norm_k < 0.1f) return 0; 
	norm_k = 1.0f/norm_k;        // use reciprocal for division
	k_init[0] *= norm_k;
	k_init[1] *= norm_k;
	k_init[2] *= norm_k;
	
	float mag_k=mag[0]*k_init[0]+mag[1]*k_init[1]+mag[2]*k_init[2];
	float i_init[3];
	for (int i = 0; i < 3; i++) {
		i_init[i] = mag[i] - mag_k*k_init[i];
	}
	vector_3_norm(i_init);
	
	float j_init[3];
	j_init[0] = k_init[1]*i_init[2]-k_init[2]*i_init[1];
	j_init[1] = k_init[2]*i_init[0]-k_init[0]*i_init[2];
	j_init[2] = k_init[0]*i_init[1]-k_init[1]*i_init[0];
	
	float R_int[3][3];
	for (int i = 0; i < 3; i++) {
		R_int[0][i] = i_init[i];
		R_int[1][i] = j_init[i];
		R_int[2][i] = k_init[i];
	}
	
//	printf("int0:%6.3f,%6.3f,%6.3f,%6.3f\r\n",quat_ahrs[0],quat_ahrs[1],quat_ahrs[2],quat_ahrs[3]);
	quaternion_dcm(R_int, quat_ahrs);
//	printf("int1:%6.3f,%6.3f,%6.3f,%6.3f\r\n",quat_ahrs[0],quat_ahrs[1],quat_ahrs[2],quat_ahrs[3]);
	
	inited_ahrs = true;
	return true;
}


void update_AHRS(float gyro[3],float acc[3],float mag[3],float dt)
{
		float norm;
		float hx, hy, hz, bx, by, bz;
		float vx, vy, vz, wx, wy, wz;
		float ex, ey, ez;
		float pa, pb, pc;
	
		float q1 = quat_ahrs[0];
		float q2 = quat_ahrs[1];
		float q3 = quat_ahrs[2];
		float q4 = quat_ahrs[3];

		// Auxiliary variables to avoid repeated arithmetic
		float q1q1 = q1 * q1;
		float q1q2 = q1 * q2;
		float q1q3 = q1 * q3;
		float q1q4 = q1 * q4;
		float q2q2 = q2 * q2;
		float q2q3 = q2 * q3;
		float q2q4 = q2 * q4;
		float q3q3 = q3 * q3;
		float q3q4 = q3 * q4;
		float q4q4 = q4 * q4;   

		// Normalise accelerometer measurement
		norm = (float)sqrtf(acc[0] * acc[0] + acc[1] * acc[1] + acc[2] * acc[2]);
		if (norm < 0.1f) return; 
		norm = 1 / norm;        // use reciprocal for division
		acc[0] *= norm;
		acc[1] *= norm;
		acc[2] *= norm;

		// Normalise magnetometer measurement
		norm = (float)sqrtf(mag[0] * mag[0] + mag[1] * mag[1] + mag[2] * mag[2]);
		if (norm < 0.1f) return; 
		norm = 1 / norm;        // use reciprocal for division
		mag[0] *= norm;
		mag[1] *= norm;
		mag[2] *= norm;

		// Reference direction of Earth's magnetic field
		hx = 2.0f * mag[0] * (0.5f - q3q3 - q4q4) + 2.0f * mag[1] * (q2q3 - q1q4) + 2.0f * mag[2] * (q2q4 + q1q3);
		hy = 2.0f * mag[0] * (q2q3 + q1q4) + 2.0f * mag[1] * (0.5f - q2q2 - q4q4) + 2.0f * mag[2] * (q3q4 - q1q2);
		hz = 2.0f * mag[0] * (q2q4 - q1q3) + 2.0f * mag[1] * (q3q4 + q1q2) + 2.0f * mag[2] * (0.5f - q2q2 - q3q3);
		bx = (float)sqrtf((hx * hx) + (hy * hy));
		bz = hz;
		
		// Estimated direction of gravity and magnetic field
		wx = 2.0f * bx * (0.5f - q3q3 - q4q4) + 2.0f * bz * (q2q4 - q1q3);
		wy = 2.0f * bx * (q2q3 - q1q4) + 2.0f * bz * (q1q2 + q3q4);
		wz = 2.0f * bx * (q1q3 + q2q4) + 2.0f * bz * (0.5f - q2q2 - q3q3);

		vx = 2.0f * (q2q4 - q1q3);
		vy = 2.0f * (q1q2 + q3q4);
		vz = q1q1 - q2q2 - q3q3 + q4q4;
		
		// Error is cross product between estimated direction and measured direction of gravity
		ex = (acc[1] * vz - acc[2] * vy);
		ey = (acc[2] * vx - acc[0] * vz);
//		ez = (acc[0] * vy - acc[1] * vx) - (mag[0] * wy - mag[1] * wx);
		ez = (acc[0] * vy - acc[1] * vx);
		if (KI_AHRS_DEF > 0.0f){
				eInt[0] += ex*dt;      // accumulate integral error
				eInt[1] += ey*dt;
				eInt[2] += ez*dt;
		}else{
				eInt[0] = 0.0f;     // prevent integral wind up
				eInt[1] = 0.0f;
				eInt[2] = 0.0f;
		}
		
		float ez_mag = -(mag[0] * wy - mag[1] * wx);
		if (KI_MAG_AHRS_DEF > 0.0f){
//				eInt_mag[0] += ex_mag*dt;      // accumulate integral error
//				eInt_mag[1] += ey_mag*dt;
				eInt_mag[2] += ez_mag*dt;
		}else{
//				eInt_mag[0] = 0.0f;     // prevent integral wind up
//				eInt_mag[1] = 0.0f;
				eInt_mag[2] = 0.0f;
		}

		// Apply feedback terms	
		gyro[0] = gyro[0] - KP_AHRS_DEF * ex - KI_AHRS_DEF * eInt[0];
		gyro[1] = gyro[1] - KP_AHRS_DEF * ey - KI_AHRS_DEF * eInt[1];
		gyro[2] = gyro[2] - KP_AHRS_DEF * ez - KI_AHRS_DEF * eInt[2];
		gyro[2] = gyro[2] - KP_MAG_AHRS_DEF * ez_mag - KI_MAG_AHRS_DEF * eInt_mag[2];

		// Integrate rate of change of quaternion
		pa = q2;
		pb = q3;
		pc = q4;
		float pd = q1;
		q1 = pd + (-q2 * gyro[0] - q3 * gyro[1] - q4 * gyro[2]) * (0.5f * dt);
		q2 = pa + (pd * gyro[0] + pb * gyro[2] - pc * gyro[1]) * (0.5f * dt);
		q3 = pb + (pd * gyro[1] - pa * gyro[2] + pc * gyro[0]) * (0.5f * dt);
		q4 = pc + (pd * gyro[2] + pa * gyro[1] - pb * gyro[0]) * (0.5f * dt);
		
		// Normalise quaternion
		norm = sqrtf(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);
		norm = 1.0f / norm;
		quat_ahrs[0] = q1 * norm;
		quat_ahrs[1] = q2 * norm;
		quat_ahrs[2] = q3 * norm;
		quat_ahrs[3] = q4 * norm;
}


#define M_PI_F		3.141592653f

void get_angle_rpy(float angle_rpy[3])
{
	angle_rpy[0] = 
	atan2f(2.0f*(quat_ahrs[2]*quat_ahrs[3]+quat_ahrs[0]*quat_ahrs[1] ), 
	quat_ahrs[0]*quat_ahrs[0]-quat_ahrs[1]*quat_ahrs[1]-quat_ahrs[2]*quat_ahrs[2]+quat_ahrs[3]*quat_ahrs[3]);

	angle_rpy[1] = -asinf(2.0f*(quat_ahrs[1]*quat_ahrs[3]-quat_ahrs[0]*quat_ahrs[2]));

	angle_rpy[2] = atan2f(2.0f*(quat_ahrs[1]*quat_ahrs[2]+quat_ahrs[0]*quat_ahrs[3] ) , 
	quat_ahrs[0]*quat_ahrs[0]+quat_ahrs[1]*quat_ahrs[1]-quat_ahrs[2]*quat_ahrs[2]-quat_ahrs[3]*quat_ahrs[3]);

//	printf("q:%6.3f,%6.3f,%6.3f,%6.3f\r\n",quat_ahrs[0],quat_ahrs[1],quat_ahrs[2],quat_ahrs[3]);
}

static float wrap_pi(float angle)
{
	if (angle > (3.0f * M_PI_F) || angle < (-3.0f * M_PI_F)) {
        // for large numbers use modulus
        angle = fmod(angle,(2.0f * M_PI_F));
    }
    if (angle > M_PI_F) { angle -= (2.0f * M_PI_F); }
    if (angle < -M_PI_F) { angle += (2.0f * M_PI_F); }
    return angle;
}

static void vector_3_norm(float vec[3])
{
	float norm_vec;
	norm_vec = sqrtf(vec[0]*vec[0]+vec[1]*vec[1]+vec[2]*vec[2]);
	if(norm_vec > 0.0000001f){
		norm_vec = 1.0f/norm_vec;        // use reciprocal for division
		vec[0] *= norm_vec;
		vec[1] *= norm_vec;
		vec[2] *= norm_vec;
	}else{
		vec[0] = 0.0f;
		vec[1] = 0.0f;
		vec[2] = 0.0f;
	}
}


static void quaternion_dcm(float R[3][3], float q[4])
{
		float t = R[0][0]*R[0][0]+R[1][1]*R[1][1]+R[2][2]*R[2][2];
		if (t > 0.0f) {
				t = sqrtf(1.0f + t);
				q[0] = 0.5f * t;
				t = 0.5f / t;
				q[1] = (R[2][1] - R[1][2]) * t;
				q[2] = (R[0][2] - R[2][0]) * t;
				q[3] = (R[1][0] - R[0][1]) * t;
		} else if (R[0][0] > R[1][1] && R[0][0] > R[2][2]) {
				t = sqrtf(1.0f + R[0][0] - R[1][1] - R[2][2]);
				q[1] = 0.5f * t;
				t = 0.5f / t;
				q[0] = (R[2][1] - R[1][2]) * t;
				q[2] = (R[1][0] + R[0][1]) * t;
				q[3] = (R[0][2] + R[2][0]) * t;
		} else if (R[1][1] > R[2][2]) {
				t = sqrtf(1.0f - R[0][0] + R[1][1] - R[2][2]);
				q[2] = 0.5f * t;
				t = 0.5f / t;
				q[0] = (R[0][2] - R[2][0]) * t;
				q[1] = (R[1][0] + R[0][1]) * t;
				q[3] = (R[2][1] + R[1][2]) * t;
		} else {
				t = sqrtf(1.0f - R[0][0] - R[1][1] + R[2][2]);
				q[3] = 0.5f * t;
				t = 0.5f / t;
				q[0] = (R[1][0] - R[0][1]) * t;
				q[1] = (R[0][2] + R[2][0]) * t;
				q[2] = (R[2][1] + R[1][2]) * t;
		}
}



