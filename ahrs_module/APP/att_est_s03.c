
#include <stdio.h>
#include <string.h>

#include <math.h>

#include "icm20602.h"
#include "IST8310.h"
#include "system.h"

//#include "att_est.h"
#include "att_est_so3.h"

#define M_PI_F		3.141592653f

//! Auxiliary variables to reduce number of repeated operations
static float q0 = 1.0f, q1 = 0.0f, q2 = 0.0f, q3 = 0.0f;	/** quaternion of sensor frame relative to auxiliary frame */
static float dq0 = 0.0f, dq1 = 0.0f, dq2 = 0.0f, dq3 = 0.0f;	/** quaternion of sensor frame relative to auxiliary frame */
static float gyro_bias[3] = {0.0f, 0.0f, 0.0f}; /** bias estimation */
static float q0q0, q0q1, q0q2, q0q3;
static float q1q1, q1q2, q1q3;
static float q2q2, q2q3;
static float q3q3;
static bool bFilterInit = false;


void quater_to_dcm(float q[4],float dcm[3][3]);
void euler_from_dcm(float dcm[3][3],float euler[3]);

// Fast inverse square-root
// See: http://en.wikipedia.org/wiki/Fast_inverse_square_root
float invSqrt(float number) 
{
    volatile long i;
    volatile float x, y;
    volatile const float f = 1.5F;

    x = number * 0.5F;
    y = number;
    i = * (( long * ) &y);
    i = 0x5f375a86 - ( i >> 1 );
    y = * (( float * ) &i);
    y = y * ( f - ( x * y * y ) );
    return y;
}

//! Using accelerometer, sense the gravity vector.
//! Using magnetometer, sense yaw.
bool init_AHRS(float gyro[3],float acc[3],float mag[3])
{
		if(bFilterInit) return true;
		
	  static uint32_t counter_init = 0;
		counter_init++;
		if(counter_init<30) return false;
	
		float ax, ay, az, mx, my, mz;
	ax = -acc[0];
	ay = -acc[1];
	az = -acc[2];
	
	mx = mag[0];
	my = mag[1];
	mz = mag[2];
			
    float initialRoll, initialPitch;
    float cosRoll, sinRoll, cosPitch, sinPitch;
    float magX, magY;
    float initialHdg, cosHeading, sinHeading;

    initialRoll = atan2(-ay, -az);
    initialPitch = atan2(ax, -az);

    cosRoll = cosf(initialRoll);
    sinRoll = sinf(initialRoll);
    cosPitch = cosf(initialPitch);
    sinPitch = sinf(initialPitch);

    magX = mx * cosPitch + my * sinRoll * sinPitch + mz * cosRoll * sinPitch;

    magY = my * cosRoll - mz * sinRoll;

    initialHdg = atan2f(-magY, magX);

    cosRoll = cosf(initialRoll * 0.5f);
    sinRoll = sinf(initialRoll * 0.5f);

    cosPitch = cosf(initialPitch * 0.5f);
    sinPitch = sinf(initialPitch * 0.5f);

    cosHeading = cosf(initialHdg * 0.5f);
    sinHeading = sinf(initialHdg * 0.5f);

    q0 = cosRoll * cosPitch * cosHeading + sinRoll * sinPitch * sinHeading;
    q1 = sinRoll * cosPitch * cosHeading - cosRoll * sinPitch * sinHeading;
    q2 = cosRoll * sinPitch * cosHeading + sinRoll * cosPitch * sinHeading;
    q3 = cosRoll * cosPitch * sinHeading - sinRoll * sinPitch * cosHeading;

    // auxillary variables to reduce number of repeated operations, for 1st pass
    q0q0 = q0 * q0;
    q0q1 = q0 * q1;
    q0q2 = q0 * q2;
    q0q3 = q0 * q3;
    q1q1 = q1 * q1;
    q1q2 = q1 * q2;
    q1q3 = q1 * q3;
    q2q2 = q2 * q2;
    q2q3 = q2 * q3;
    q3q3 = q3 * q3;
		
		bFilterInit = true;
		return true;
}

void update_AHRS(float gyro[3],float acc[3],float mag[3],float dt)
{
	
	float gx, gy, gz, ax, ay, az, mx, my, mz;
	gx=gyro[0];
	gy=gyro[1];
	gz=gyro[2];
	
	ax=-acc[0];
	ay=-acc[1];
	az=-acc[2];
	
	mx=mag[0];
	my=mag[1];
	mz=mag[2];
	
	float twoKp = 0.8f;
	float twoKi = 0.002f;
	
	float recipNorm;
	float halfex = 0.0f, halfey = 0.0f, halfez = 0.0f;
        	
	//! If magnetometer measurement is available, use it.
	float norm_mag=sqrtf(mx*mx+my*my+mz*mz);
	if(norm_mag>0.0f) {
		float hx, hy, hz, bx, bz;
		float halfwx, halfwy, halfwz;
	
		// Normalise magnetometer measurement
		// Will sqrt work better? PX4 system is powerful enough?
    	recipNorm = invSqrt(mx * mx + my * my + mz * mz);
    	mx *= recipNorm;
    	my *= recipNorm;
    	mz *= recipNorm;
    
    	// Reference direction of Earth's magnetic field
    	hx = 2.0f * (mx * (0.5f - q2q2 - q3q3) + my * (q1q2 - q0q3) + mz * (q1q3 + q0q2));
    	hy = 2.0f * (mx * (q1q2 + q0q3) + my * (0.5f - q1q1 - q3q3) + mz * (q2q3 - q0q1));
			hz = 2.0f * mx * (q1q3 - q0q2) + 2.0f * my * (q2q3 + q0q1) + 2.0f * mz * (0.5f - q1q1 - q2q2);
		
//		   	hx = (mx * (0.5f - q2q2 - q3q3) + my * (q1q2 - q0q3) + mz * (q1q3 + q0q2));
//    	hy = (mx * (q1q2 + q0q3) + my * (0.5f - q1q1 - q3q3) + mz * (q2q3 - q0q1));
//		hz = mx * (q1q3 - q0q2) + my * (q2q3 + q0q1) + mz * (0.5f - q1q1 - q2q2);
		
    	bx = sqrtf(hx * hx + hy * hy);
    	bz = hz;
    
    	// Estimated direction of magnetic field
    	halfwx = bx * (0.5f - q2q2 - q3q3) + bz * (q1q3 - q0q2);
    	halfwy = bx * (q1q2 - q0q3) + bz * (q0q1 + q2q3);
    	halfwz = bx * (q0q2 + q1q3) + bz * (0.5f - q1q1 - q2q2);
    
    	// Error is sum of cross product between estimated direction and measured direction of field vectors
    	halfex += (my * halfwz - mz * halfwy);
    	halfey += (mz * halfwx - mx * halfwz);
    	halfez += (mx * halfwy - my * halfwx);
	}

	float norm_acc=sqrtf(ax*ax+ay*ay+az*az);
	// Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
	if(norm_acc>0.001f) {
		float halfvx, halfvy, halfvz;
	
		// Normalise accelerometer measurement
		recipNorm = invSqrt(ax * ax + ay * ay + az * az);
		ax *= recipNorm;
		ay *= recipNorm;
		az *= recipNorm;

		// Estimated direction of gravity and magnetic field
		halfvx = q1q3 - q0q2;
		halfvy = q0q1 + q2q3;
		halfvz = q0q0 - 0.5f + q3q3;
	
		// Error is sum of cross product between estimated direction and measured direction of field vectors
		halfex += ay * halfvz - az * halfvy;
		halfey += az * halfvx - ax * halfvz;
		halfez += ax * halfvy - ay * halfvx;
		
	}

	// Apply feedback only when valid data has been gathered from the accelerometer or magnetometer
	float norm_halfex=sqrtf(halfex*halfex+halfey*halfey+halfez*halfez);
	if(norm_halfex>0.001f) {
		// Compute and apply integral feedback if enabled
		if(twoKi > 0.0f) {
			gyro_bias[0] += twoKi * halfex * dt;	// integral error scaled by Ki
			gyro_bias[1] += twoKi * halfey * dt;
			gyro_bias[2] += twoKi * halfez * dt;
			
			// apply integral feedback
			gx += gyro_bias[0];
			gy += gyro_bias[1];
			gz += gyro_bias[2];
		}else {
			gyro_bias[0] = 0.0f;	// prevent integral windup
			gyro_bias[1] = 0.0f;
			gyro_bias[2] = 0.0f;
		}

		// Apply proportional feedback
		gx += twoKp * halfex;
		gy += twoKp * halfey;
		gz += twoKp * halfez;
	}
	
	//! Integrate rate of change of quaternion
#if 0
	gx *= (0.5f * dt);		// pre-multiply common factors
	gy *= (0.5f * dt);
	gz *= (0.5f * dt);
#endif 

	// Time derivative of quaternion. q_dot = 0.5*q\otimes omega.
	//! q_k = q_{k-1} + dt*\dot{q}
	//! \dot{q} = 0.5*q \otimes P(\omega)
	dq0 = 0.5f*(-q1 * gx - q2 * gy - q3 * gz);
	dq1 = 0.5f*(q0 * gx + q2 * gz - q3 * gy);
	dq2 = 0.5f*(q0 * gy - q1 * gz + q3 * gx);
	dq3 = 0.5f*(q0 * gz + q1 * gy - q2 * gx); 

	q0 += dt*dq0;
	q1 += dt*dq1;
	q2 += dt*dq2;
	q3 += dt*dq3;
	
	// Normalise quaternion
	recipNorm = invSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
	q0 *= recipNorm;
	q1 *= recipNorm;
	q2 *= recipNorm;
	q3 *= recipNorm;

	// Auxiliary variables to avoid repeated arithmetic
    q0q0 = q0 * q0;
    q0q1 = q0 * q1;
    q0q2 = q0 * q2;
    q0q3 = q0 * q3;
    q1q1 = q1 * q1;
    q1q2 = q1 * q2;
   	q1q3 = q1 * q3;
    q2q2 = q2 * q2;
    q2q3 = q2 * q3;
    q3q3 = q3 * q3;   
}

void get_angle_rpy(float angle_rpy[3])
{
#if 0
	angle_rpy[0] = 
	atan2f(2.0f*(q2*q3+q0*q1 ), 
	q0*q0-q1*q1-q2*q2+q3*q3);

	angle_rpy[1] = -asinf(2.0f*(q1*q3-q0*q2));

	angle_rpy[2] = atan2f(2.0f*(q1*q2+q0*q3 ) , 
	q0*q0+q1*q1-q2*q2-q3*q3);
#endif
	
	float dcm[3][3];
	float quat_ahrs[4]={q0,q1,q2,q3};
	quater_to_dcm(quat_ahrs,dcm);
	euler_from_dcm(dcm,angle_rpy);
//	printf("q:%6.3f,%6.3f,%6.3f,%6.3f\r\n",q[0],q[1],q[2],q[3]);
}


#if 1
void quater_to_dcm(float q[4],float dcm[3][3])
{
		float a = q[0];
		float b = q[1];
		float c = q[2];
		float d = q[3];
		float aa = a * a;
		float ab = a * b;
		float ac = a * c;
		float ad = a * d;
		float bb = b * b;
		float bc = b * c;
		float bd = b * d;
		float cc = c * c;
		float cd = c * d;
		float dd = d * d;
		dcm[0][0] = aa + bb - cc - dd;
		dcm[0][1] = 2.0f * (bc - ad);
		dcm[0][2] = 2.0f * (ac + bd);
		dcm[1][0] = 2.0f * (bc + ad);
		dcm[1][1] = aa - bb + cc - dd;
		dcm[1][2] = 2.0f * (cd - ab);
		dcm[2][0] = 2.0f * (bd - ac);
		dcm[2][1] = 2.0f * (ab + cd);
		dcm[2][2] = aa - bb - cc + dd;
}

void euler_from_dcm(float dcm[3][3],float euler[3])
{
		float phi_val = atan2f(dcm[2][1], dcm[2][2]);
		float theta_val = asinf(-dcm[2][0]);
		float psi_val = atan2f(dcm[1][0], dcm[0][0]);
		float pi = M_PI_F;

		if (fabsf(theta_val - 0.5f*pi) < 0.001f) {
				phi_val = 0.0f;
				psi_val = atan2f(dcm[1][2], dcm[0][2]);

		} else if (fabsf(theta_val + 0.5f*pi) < 0.001f) {
				phi_val = 0.0f;
				psi_val = atan2f(-dcm[1][2], -dcm[0][2]);
		}

		euler[0] = phi_val;
		euler[1] = theta_val;
		euler[2] = psi_val;
}
		
#endif
