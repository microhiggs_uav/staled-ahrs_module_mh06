

#ifndef _MAHONYAHRS_H_
#define _MAHONYAHRS_H_

#include <stdint.h>
#include <stdbool.h>

bool init_AHRS(float gyro[3],float acc[3],float mag[3]);

void update_AHRS(float gyro[3],float acc[3],float mag[3],float dt);

void get_angle_rpy(float angle_rpy[3]);

#endif






