
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "system.h"
#include "flash.h"
#include "icm20602.h"
#include "IST8310.h"
#include "calibrator.h"
#include "sphere_fit.h"
#include "att_est.h"

#define OFFSET_PARM_FLASH		0x19000		// 0x6320
#define BASE_PARAM_FLASH		(FLASH_BASE+OFFSET_PARM_FLASH)

#define	CONSTANTS_ONE_G		9.81f

#define ACC_SAMPLES_NUM  100
#define GYRO_SAMPLES_NUM 100
#define MAG_SAMPLES_NUM  600

struct calibrate_status_s calib_status={CALIB_STATUS_CALIBED,CALIB_STATUS_CALIBED,CALIB_STATUS_CALIBED,0,0,0};

int    acc_samples_counter = 0;
int    gyro_samples_counter = 0;
int    mag_samples_counter = 0;

static float int_acc[3] = {0.0f};
static float int_gyro[3] = {0.0f};

float accel_offset[3]={0.0f};
float gyro_offset[3]={0.0f};
float mag_offset[3]={0.0f};
float mag_scale[3]={1.0f,1.0f,1.0f};
float mag_offdiag[3]={0.0f};

void gyro_param_calib_save(float gyro[3]);
void acc_param_calib_save(float acc[3]);
void mag_param_calib_save(float mag[3]);

enum calibrate_return calibrate_from_orientation(float acc[3],float mag_raw[3]);

static float mag_corr[3] = {0.0f};

/******* acc calibration ********/
void acc_calib_start()
{
	if(calib_status.acc != CALIB_STATUS_DOING){
		calib_status.acc = CALIB_STATUS_START;
	}
}


static int counter_acc = 0;
int acc_calib_apply(float acc_raw[3])
{
	counter_acc++;
	if(counter_acc%60==0){
		int_acc[0] += acc_raw[0];
		int_acc[1] += acc_raw[1];
		int_acc[2] += acc_raw[2];
		acc_samples_counter++;
	}
	calib_status.acc_progress = acc_samples_counter * 100 / ACC_SAMPLES_NUM;	

	if(acc_samples_counter >= ACC_SAMPLES_NUM){
		accel_offset[0] = int_acc[0]/acc_samples_counter;
		accel_offset[1] = int_acc[1]/acc_samples_counter;
		accel_offset[2] = int_acc[2]/acc_samples_counter;
    accel_offset[2] += CONSTANTS_ONE_G;
		
		counter_acc = 0;
		printf("acc offset:%6.3f,%6.3f,%6.3f---%d\r\n",accel_offset[0],accel_offset[1],accel_offset[2],acc_samples_counter);
	  return 1;
	}else{
		return 0;
	}
}

void acc_calib_update()
{ 
	float acc_imu[3];
	switch(calib_status.acc){
		case CALIB_STATUS_START:
			acc_samples_counter = 0; 
			memset(int_acc,0x00,sizeof(int_acc));				
			calib_status.acc = CALIB_STATUS_DOING;
			printf("acc calibration started\r\n");
			break;
		case CALIB_STATUS_DOING:
			imu_acc_get(acc_imu);
			if(acc_calib_apply(acc_imu) == 1){
				acc_param_calib_save(accel_offset);
				printf("acc calibration done\r\n");
				calib_status.acc = CALIB_STATUS_CALIBED;
			}
			break;
		default:
			break;
   } 
}


/******* gyro calibration ********/
void gyro_calib_start()
{
	if(calib_status.gyro != CALIB_STATUS_DOING){
		calib_status.gyro = CALIB_STATUS_START;
	}
}

static int counter_gyro = 0;
int gyro_calib_apply(float gyro_raw[3])
{
	counter_gyro++;
	if(counter_gyro%60==0){
		int_gyro[0] += gyro_raw[0];
		int_gyro[1] += gyro_raw[1];
		int_gyro[2] += gyro_raw[2];
		gyro_samples_counter++;
	}
	calib_status.gyro_progress = gyro_samples_counter * 100 / GYRO_SAMPLES_NUM;	

	if(gyro_samples_counter >= GYRO_SAMPLES_NUM){
		gyro_offset[0] = int_gyro[0]/gyro_samples_counter;
		gyro_offset[1] = int_gyro[1]/gyro_samples_counter;
		gyro_offset[2] = int_gyro[2]/gyro_samples_counter;
		
		counter_gyro = 0;
		printf("gyro offset:%6.3f,%6.3f,%6.3f---%d\r\n",gyro_offset[0],gyro_offset[1],gyro_offset[2],gyro_samples_counter);
	  return 1;
	}else{
		return 0;
	}

}

void gyro_calib_update()
{ 
	float gyro_imu[3];
	switch(calib_status.gyro){
		case CALIB_STATUS_START:
			gyro_samples_counter = 0; 
			memset(int_gyro,0x00,sizeof(int_gyro));
			calib_status.gyro = CALIB_STATUS_DOING;
			printf("gyro calibration started\r\n");
			break;
		case CALIB_STATUS_DOING:
			imu_gyro_get(gyro_imu);
			if(gyro_calib_apply(gyro_imu) == 1){
				gyro_param_calib_save(gyro_offset);
				printf("gyro calibration done\r\n");
				calib_status.gyro = CALIB_STATUS_CALIBED;
			}
			break;
		default:
			break;
   } 
}

void imu_calib_start()
{
	gyro_calib_start();
	acc_calib_start();
}

void imu_calib_update()
{ 
	gyro_calib_update();
	acc_calib_update();
} 

/******* mag calibration ********/
float *magx_samples = NULL;
float *magy_samples = NULL;
float *magz_samples = NULL;
float sphere_radius = 0.0f;
void mag_calib_start()
{
	if(calib_status.mag != CALIB_STATUS_DOING){
		calib_status.mag = CALIB_STATUS_START;
	}
}


static int counter_mag = 0;
int mag_calib_apply(float mag_raw[3])
{
	counter_mag++;
	if(counter_mag%30==0){
		magx_samples[mag_samples_counter] = mag_raw[0];
		magy_samples[mag_samples_counter] = mag_raw[1];
		magz_samples[mag_samples_counter] = mag_raw[2];
		mag_samples_counter++;
	}
	calib_status.mag_progress = mag_samples_counter * 100 / MAG_SAMPLES_NUM;	

	if(mag_samples_counter >= MAG_SAMPLES_NUM){
		sphere_fit_least_squares(magx_samples,magy_samples,magz_samples,mag_samples_counter, 1000, 0.0f, &mag_offset[0],&mag_offset[1],&mag_offset[2], &sphere_radius);
		printf("mag offset0:%6.3f,%6.3f,%6.3f\r\n",mag_offset[0],mag_offset[1],mag_offset[2]);
//		mag_offdiag[0]=mag_offdiag[1]=mag_offdiag[2]=sphere_radius;
		printf("mag scalel0:%6.3f,%6.3f,%6.3f\r\n",mag_scale[0],mag_scale[1],mag_scale[2]);
//		ellipsoid_fit_least_squares(magx_samples, magy_samples, magz_samples,
//				MAG_SAMPLES_NUM, 15, 0.0f, &mag_offset[0],&mag_offset[1],&mag_offset[2],
//				&sphere_radius, &mag_scale[0],&mag_scale[1],&mag_scale[2], &mag_offdiag[0], &mag_offdiag[1], &mag_offdiag[2]);
		counter_mag = 0;
//		printf("mag offset1:%6.3f,%6.3f,%6.3f\r\n",mag_offset[0],mag_offset[1],mag_offset[2]);
//		printf("mag scalel1:%6.3f,%6.3f,%6.3f\r\n",mag_scale[0],mag_scale[1],mag_scale[2]);
	  return 1;
	}else{
		return 0;
	}

}

void mag_calib_update()
{ 
	float mag_raw[3];
	float acc_raw[3];
	switch(calib_status.mag){
		case CALIB_STATUS_START:
			mag_samples_counter = 0; 
			magx_samples = (float *)malloc(MAG_SAMPLES_NUM * sizeof(float)); 
			magy_samples = (float *)malloc(MAG_SAMPLES_NUM * sizeof(float)); 
			magz_samples = (float *)malloc(MAG_SAMPLES_NUM * sizeof(float)); 
		
			if (NULL == magx_samples || NULL == magy_samples || NULL == magz_samples){
				calib_status.mag = CALIB_STATUS_FAILED;
				break;
			}				
			calib_status.mag = CALIB_STATUS_DOING;
			printf("mag calibration started\r\n");
			break;
		case CALIB_STATUS_DOING:
			mag_value_get(mag_raw);
			imu_acc_get(acc_raw);
//			if(mag_calib_apply(mag_raw) == 1){
			if(calibrate_from_orientation(acc_raw,mag_raw)==calibrate_return_ok){
				mag_param_calib_save(mag_offset);
				calib_status.mag = CALIB_STATUS_CALIBED;
				free(magx_samples); 
				magx_samples= NULL; 
				free(magy_samples); 
				magy_samples= NULL; 
				free(magz_samples); 
				magz_samples= NULL;			
				printf("mag calibration done\r\n\r\n");
			}
			break;
		default:
			break;
   } 
}

float gyro_calibrated[3] = {0.0f};
float accel_calibrated[3] = {0.0f};
float mag_calibrated[3] = {0.0f};
void calib_update(float gyro[3],float acc[3],float mag[3])
{
	imu_calib_update();
	mag_calib_update();
	
	if(calib_status.gyro == CALIB_STATUS_CALIBED){
		gyro[0] -= gyro_offset[0];
		gyro[1] -= gyro_offset[1];
		gyro[2] -= gyro_offset[2];
		memcpy(gyro_calibrated,gyro,sizeof(gyro_calibrated));
	}
	
	if(calib_status.acc == CALIB_STATUS_CALIBED){
		acc[0] -= accel_offset[0];
		acc[1] -= accel_offset[1];
		acc[2] -= accel_offset[2];
		memcpy(accel_calibrated,acc,sizeof(accel_calibrated));
	}
	
	if(calib_status.mag == CALIB_STATUS_CALIBED){
		mag[0] -= mag_offset[0];
		mag[1] -= mag_offset[1];
		mag[2] -= mag_offset[2];
		memcpy(mag_calibrated,mag,sizeof(mag_calibrated));
	}
	memcpy(mag_corr,mag,sizeof(mag_corr));
}

void imu_offset_get(float gyro[3],float acc[3])
{
	memcpy(gyro, gyro_offset, sizeof(gyro_offset));
	memcpy(acc, accel_offset, sizeof(accel_offset));
}

void mag_offset_get(float mag[3])
{
	memcpy(mag, mag_offset, sizeof(mag_offset));
}


void imu_calibrated_get(float gyro[3],float acc[3])
{
	memcpy(gyro, gyro_calibrated, sizeof(gyro_calibrated));
	memcpy(acc, accel_calibrated, sizeof(accel_calibrated));
}

void gyro_param_calib_save(float gyro[3])
{
	int32_t offset_data[9];
	offset_data[0]=1000*gyro[0]; 
	offset_data[1]=1000*gyro[1]; 
	offset_data[2]=1000*gyro[2]; 
	
	offset_data[3]=1000*accel_offset[0]; 
	offset_data[4]=1000*accel_offset[1]; 
	offset_data[5]=1000*accel_offset[2]; 
	
	offset_data[6]=1000*mag_offset[0]; 
	offset_data[7]=1000*mag_offset[1]; 
	offset_data[8]=1000*mag_offset[2]; 
	
	FLASH_WriteData(BASE_PARAM_FLASH,offset_data,9);
	printf("gyro offset saved:%6.3f,%6.3f,%6.3f\r\n",0.001f*offset_data[0],0.001f*offset_data[1],0.001f*offset_data[2]);
	printf("acc offset saved:%6.3f,%6.3f,%6.3f\r\n",0.001f*offset_data[3],0.001f*offset_data[4],0.001f*offset_data[5]);
	printf("mag offset saved:%6.3f,%6.3f,%6.3f\r\n",0.001f*offset_data[6],0.001f*offset_data[7],0.001f*offset_data[8]);
}

void acc_param_calib_save(float acc[3])
{
	int32_t offset_data[9];
	offset_data[0]=1000*gyro_offset[0]; 
	offset_data[1]=1000*gyro_offset[1]; 
	offset_data[2]=1000*gyro_offset[2]; 
	
	offset_data[3]=1000*acc[0]; 
	offset_data[4]=1000*acc[1]; 
	offset_data[5]=1000*acc[2]; 
	
	offset_data[6]=1000*mag_offset[0]; 
	offset_data[7]=1000*mag_offset[1]; 
	offset_data[8]=1000*mag_offset[2]; 
	
	FLASH_WriteData(BASE_PARAM_FLASH,offset_data,9);
	printf("gyro offset saved:%6.3f,%6.3f,%6.3f\r\n",0.001f*offset_data[0],0.001f*offset_data[1],0.001f*offset_data[2]);
	printf("acc offset saved:%6.3f,%6.3f,%6.3f\r\n",0.001f*offset_data[3],0.001f*offset_data[4],0.001f*offset_data[5]);
	printf("mag offset saved:%6.3f,%6.3f,%6.3f\r\n",0.001f*offset_data[6],0.001f*offset_data[7],0.001f*offset_data[8]);
}


void mag_param_calib_save(float mag[3])
{
	int32_t offset_data[9];
	offset_data[0]=1000*gyro_offset[0]; 
	offset_data[1]=1000*gyro_offset[1]; 
	offset_data[2]=1000*gyro_offset[2]; 
	
	offset_data[3]=1000*accel_offset[0]; 
	offset_data[4]=1000*accel_offset[1]; 
	offset_data[5]=1000*accel_offset[2]; 
	
	offset_data[6]=1000*mag[0]; 
	offset_data[7]=1000*mag[1]; 
	offset_data[8]=1000*mag[2]; 
	
	FLASH_WriteData(BASE_PARAM_FLASH,offset_data,9);
	printf("mag offset saved:%6.3f,%6.3f,%6.3f\r\n",mag[0],mag[1],mag[2]);
}

void param_calib_load(float gyro[3],float acc[3],float mag[3])
{
	int32_t offset_data[9];
	FLASH_ReadData(BASE_PARAM_FLASH,offset_data,9);
	printf("mag_load:%d,%d,%d\r\n",offset_data[6],offset_data[7],offset_data[8]);
	
	gyro[0]=0.001f*offset_data[0]; 
	gyro[1]=0.001f*offset_data[1]; 
	gyro[2]=0.001f*offset_data[2];

	acc[0]=0.001f*offset_data[3]; 
	acc[1]=0.001f*offset_data[4]; 
	acc[2]=0.001f*offset_data[5]; 

	mag[0]=0.001f*offset_data[6]; 
	mag[1]=0.001f*offset_data[7]; 
	mag[2]=0.001f*offset_data[8]; 
}


void calib_init()
{
	param_calib_load(gyro_offset,accel_offset,mag_offset);
	printf("calibration param load successfully\r\n");
	printf("offset_g:%6.3f,%6.3f,%6.3f\r\n",gyro_offset[0],gyro_offset[1],gyro_offset[2]);
	printf("offset_a:%6.3f,%6.3f,%6.3f\r\n",accel_offset[0],accel_offset[1],accel_offset[2]);
	printf("offset_m:%6.3f,%6.3f,%6.3f\r\n",mag_offset[0],mag_offset[1],mag_offset[2]);
}

struct calibrate_status_s calib_status_get()
{
	return calib_status;
}


void imu_calib_progress_get(uint8_t progress[2])
{
	float prog[2];
	prog[0] = calib_status.gyro_progress;
	prog[1] = calib_status.mag_progress;
	memcpy(progress,prog,sizeof(prog));
}

void mag_value_calibrated_get(uint8_t mag_cal[3])
{
	memcpy(mag_cal,mag_corr,sizeof(mag_corr));
}

static uint32_t counter_direction[6] = {0};
enum detect_orientation_return detect_orientation(float acc_raw[3])
{
	float		accel_ema[3] = { 0.0f };					// exponential moving average of accel
	float		accel_disp[3] = { 0.0f};					// max-hold dispersion of accel
	static float	ema_len = 0.5f;							// EMA time constant in seconds
	static float	normal_still_thr = 0.25;		// normal still threshold
	float		still_thr2 = (normal_still_thr * 3.0f)*(normal_still_thr * 3.0f);
	static float		accel_err_thr = 5.0f;			// set accel error threshold to 5m/s^2
	const float	still_time = 0.5f;					// still time required in us

	const float t_start = 0.0001f*systick_count;

	/* set timeout to 30s */
	static float timeout = 30.0f;

	float t_timeout = t_start + timeout;
	float t = 0.0001f*systick_count;
	float t_prev = t_start;
	float t_still = 0.0f;

	unsigned poll_errcount = 0;

	/* wait blocking for new data */
	float dt = (t - t_prev) / 1000000.0f;
	t_prev = t;
	float w = dt / ema_len;

	for (unsigned i = 0; i < 3; i++) {

		float di = acc_raw[i];

		float d = di - accel_ema[i];
		accel_ema[i] += d * w;
		d = d * d;
		accel_disp[i] = accel_disp[i] * (1.0f - w);

		if (d > still_thr2 * 8.0f) {
			d = still_thr2 * 8.0f;
		}

		if (d > accel_disp[i]) {
			accel_disp[i] = d;
		}
	}

	/* still detector with hysteresis */
	if (accel_disp[0] < still_thr2 &&
			accel_disp[1] < still_thr2 &&
			accel_disp[2] < still_thr2) {
		/* is still now */
		if (t_still == 0) {
			/* first time */
			printf("[cal] detected rest position, hold still...\r\n");
			t_still = t;
			t_timeout = t + timeout;

		} else {
			/* still since t_still */
			if (t > t_still + still_time) {
				/* vehicle is still, exit from the loop to detection of its orientation */
//				break;
			}
		}

	} else if (accel_disp[0] > still_thr2 * 4.0f ||
			 accel_disp[1] > still_thr2 * 4.0f ||
			 accel_disp[2] > still_thr2 * 4.0f) {
		/* not still, reset still start time */
		if (t_still != 0) {
			printf("[cal] detected motion, hold still...\r\n");
//					usleep(200000);
			t_still = 0;
		}
	}

	if (t > t_timeout) {
		poll_errcount++;
	}

	if (poll_errcount > 1000) {
		printf("calibration error, exit\r\n");
		return DETECT_ORIENTATION_ERROR;
	}

	if (fabsf(accel_ema[0] - CONSTANTS_ONE_G) < accel_err_thr &&
	    fabsf(accel_ema[1]) < accel_err_thr &&
	    fabsf(accel_ema[2]) < accel_err_thr) {
		counter_direction[DETECT_ORIENTATION_TAIL_DOWN]++;
//		return DETECT_ORIENTATION_TAIL_DOWN;        // [ g, 0, 0 ]
	}else{
		counter_direction[DETECT_ORIENTATION_TAIL_DOWN]=0;
	}

	if (fabsf(accel_ema[0] + CONSTANTS_ONE_G) < accel_err_thr &&
	    fabsf(accel_ema[1]) < accel_err_thr &&
	    fabsf(accel_ema[2]) < accel_err_thr) {
		counter_direction[DETECT_ORIENTATION_NOSE_DOWN]++;
//		return DETECT_ORIENTATION_NOSE_DOWN;        // [ -g, 0, 0 ]
	}else{
		counter_direction[DETECT_ORIENTATION_NOSE_DOWN]=0;
	}

	if (fabsf(accel_ema[0]) < accel_err_thr &&
	    fabsf(accel_ema[1] - CONSTANTS_ONE_G) < accel_err_thr &&
	    fabsf(accel_ema[2]) < accel_err_thr) {
		counter_direction[DETECT_ORIENTATION_LEFT]++;
//		return DETECT_ORIENTATION_LEFT;        // [ 0, g, 0 ]
	}else{
		counter_direction[DETECT_ORIENTATION_LEFT]=0;
	}

	if (fabsf(accel_ema[0]) < accel_err_thr &&
	    fabsf(accel_ema[1] + CONSTANTS_ONE_G) < accel_err_thr &&
	    fabsf(accel_ema[2]) < accel_err_thr) {
		counter_direction[DETECT_ORIENTATION_RIGHT]++;
//		return DETECT_ORIENTATION_RIGHT;        // [ 0, -g, 0 ]
	}else{
		counter_direction[DETECT_ORIENTATION_RIGHT]=0;
	}

	if (fabsf(accel_ema[0]) < accel_err_thr &&
	    fabsf(accel_ema[1]) < accel_err_thr &&
	    fabsf(accel_ema[2] - CONSTANTS_ONE_G) < accel_err_thr) {
		counter_direction[DETECT_ORIENTATION_UPSIDE_DOWN]++;
//		return DETECT_ORIENTATION_UPSIDE_DOWN;        // [ 0, 0, g ]
	}else{
		counter_direction[DETECT_ORIENTATION_UPSIDE_DOWN]=0;
	}

	if (fabsf(accel_ema[0]) < accel_err_thr &&
	    fabsf(accel_ema[1]) < accel_err_thr &&
	    fabsf(accel_ema[2] + CONSTANTS_ONE_G) < accel_err_thr) {
		counter_direction[DETECT_ORIENTATION_RIGHTSIDE_UP]++;
//		return DETECT_ORIENTATION_RIGHTSIDE_UP;        // [ 0, 0, -g ]
	}else{
		counter_direction[DETECT_ORIENTATION_RIGHTSIDE_UP]=0;
	}

	for (unsigned i = 0; i < 6; i++) {
		if(counter_direction[i]>6){
//			counter_direction[i]=0;
			return i; 
		}
	}
	return DETECT_ORIENTATION_ERROR;	// Can't detect orientation
}


const char *detect_orientation_str(enum detect_orientation_return orientation)
{
	static const char *rgOrientationStrs[] = {
		"back",		// tail down
		"front",	// nose down
		"left",
		"right",
		"up",			// upside-down
		"down",		// right-side up
		"error"
	};

	return rgOrientationStrs[orientation];
}

#define DELTA_ANGLE			(0.063f)		// 360/100/57.3f
#define CAL_QGC_ORIENTATION_DETECTED_MSG	"[cal] %s orientation detected"
#define CAL_QGC_SIDE_DONE_MSG							"[cal] %s side done, rotate to a different side"
static const unsigned detect_orientation_side_count = 6;
static unsigned orientation_failures = 0;
static bool	side_data_collected[detect_orientation_side_count]={false};
enum calibrate_return calibrate_from_orientation(float acc[3],float mag_raw[3])
{
//		unsigned int side_complete_count = 0;

//		// Update the number of completed sides
//		for (unsigned i = 0; i < detect_orientation_side_count; i++) {
//			if (side_data_collected[i]) {
//				side_complete_count++;
//			}
//		}

//		if (side_complete_count == detect_orientation_side_count) {
//			// We have completed all sides, move on
//			break;
//		}

		/* inform user which orientations are still needed */
//		char pendingStr[80];
//		pendingStr[0] = 0;

//		for (unsigned int cur_orientation = 0; cur_orientation < detect_orientation_side_count; cur_orientation++) {
//			if (!side_data_collected[cur_orientation]) {
//				strncat(pendingStr, " ", sizeof(pendingStr) - 1);
//				strncat(pendingStr, detect_orientation_str((enum detect_orientation_return)cur_orientation), sizeof(pendingStr) - 1);
//			}
//		}

//		printf("[cal] pending\r\n");
//		printf("[cal] hold vehicle still on a pending side\r\n");

		enum detect_orientation_return orient = detect_orientation(acc);
		
		if (orient == DETECT_ORIENTATION_ERROR) {
//			orientation_failures++;
			printf("[cal] detected motion, hold still...\r\n");
//			continue;
			return calibrate_return_error;
		}
		
		float gyro_integ[3];
		get_gyro_integral(gyro_integ);	
		static float gyro_integ_prev[3];	
		float delta_angle[3];
		delta_angle[0]=fabsf(gyro_integ[0]-gyro_integ_prev[0]);
		delta_angle[1]=fabsf(gyro_integ[1]-gyro_integ_prev[1]);
		delta_angle[2]=fabsf(gyro_integ[2]-gyro_integ_prev[2]);
		switch(orient){
			case DETECT_ORIENTATION_TAIL_DOWN:
				if(delta_angle[0]>DELTA_ANGLE){
					magx_samples[mag_samples_counter] = mag_raw[0];
					magy_samples[mag_samples_counter] = mag_raw[1];
					magz_samples[mag_samples_counter] = mag_raw[2];
					mag_samples_counter++;				
				}
				break;
			case DETECT_ORIENTATION_NOSE_DOWN:
				if(delta_angle[0]>DELTA_ANGLE){
					magx_samples[mag_samples_counter] = mag_raw[0];
					magy_samples[mag_samples_counter] = mag_raw[1];
					magz_samples[mag_samples_counter] = mag_raw[2];
					mag_samples_counter++;				
				}
				break;
			case DETECT_ORIENTATION_LEFT:
				if(delta_angle[2]>DELTA_ANGLE){
					magx_samples[mag_samples_counter] = mag_raw[0];
					magy_samples[mag_samples_counter] = mag_raw[1];
					magz_samples[mag_samples_counter] = mag_raw[2];
					mag_samples_counter++;				
				}
				break;
			case DETECT_ORIENTATION_RIGHT:
				if(delta_angle[2]>DELTA_ANGLE){
					magx_samples[mag_samples_counter] = mag_raw[0];
					magy_samples[mag_samples_counter] = mag_raw[1];
					magz_samples[mag_samples_counter] = mag_raw[2];
					mag_samples_counter++;				
				}
				break;
			case DETECT_ORIENTATION_UPSIDE_DOWN:
				if(delta_angle[2]>DELTA_ANGLE){
					magx_samples[mag_samples_counter] = mag_raw[0];
					magy_samples[mag_samples_counter] = mag_raw[1];
					magz_samples[mag_samples_counter] = mag_raw[2];
					mag_samples_counter++;				
				}
				break;
			case DETECT_ORIENTATION_RIGHTSIDE_UP:
				if(delta_angle[2]>DELTA_ANGLE){
					magx_samples[mag_samples_counter] = mag_raw[0];
					magy_samples[mag_samples_counter] = mag_raw[1];
					magz_samples[mag_samples_counter] = mag_raw[2];
					mag_samples_counter++;				
				}
				break;
			default:
				break;
		} 
		memcpy(gyro_integ_prev,gyro_integ,sizeof(gyro_integ_prev));
		
	calib_status.mag_progress = mag_samples_counter * 100 / MAG_SAMPLES_NUM;	

	if(mag_samples_counter >= MAG_SAMPLES_NUM){
		sphere_fit_least_squares(magx_samples,magy_samples,magz_samples,mag_samples_counter, 1000, 0.0f, &mag_offset[0],&mag_offset[1],&mag_offset[2], &sphere_radius);
		printf("mag offset0:%6.3f,%6.3f,%6.3f\r\n",mag_offset[0],mag_offset[1],mag_offset[2]);
//		mag_offdiag[0]=mag_offdiag[1]=mag_offdiag[2]=sphere_radius;
		printf("mag scalel0:%6.3f,%6.3f,%6.3f\r\n",mag_scale[0],mag_scale[1],mag_scale[2]);
//		ellipsoid_fit_least_squares(magx_samples, magy_samples, magz_samples,
//				MAG_SAMPLES_NUM, 15, 0.0f, &mag_offset[0],&mag_offset[1],&mag_offset[2],
//				&sphere_radius, &mag_scale[0],&mag_scale[1],&mag_scale[2], &mag_offdiag[0], &mag_offdiag[1], &mag_offdiag[2]);
		counter_mag = 0;
//		printf("mag offset1:%6.3f,%6.3f,%6.3f\r\n",mag_offset[0],mag_offset[1],mag_offset[2]);
//		printf("mag scalel1:%6.3f,%6.3f,%6.3f\r\n",mag_scale[0],mag_scale[1],mag_scale[2]);
	  return calibrate_return_ok;
	}else{
		return calibrate_return_doing;
	}

	

//		/* inform user about already handled side */
//		if (side_data_collected[orient]) {
//			orientation_failures++;
//			printf("[cal] %s side already completed\r\n", detect_orientation_str(orient));
////			continue;
//		}

//		printf(CAL_QGC_ORIENTATION_DETECTED_MSG, detect_orientation_str(orient));
//		printf(CAL_QGC_ORIENTATION_DETECTED_MSG, detect_orientation_str(orient));
//		orientation_failures = 0;

//		printf(CAL_QGC_SIDE_DONE_MSG, detect_orientation_str(orient));
//		printf(CAL_QGC_SIDE_DONE_MSG, detect_orientation_str(orient));

//		// Note that this side is complete
//		side_data_collected[orient] = true;
}
	
	
	
	
