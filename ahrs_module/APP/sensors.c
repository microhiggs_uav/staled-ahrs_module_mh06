
#include <math.h>
#include "icm20602.h"
#include "IST8310.h"
#include "sensors.h"

enum sensors_read_status_e imu_read_status = SENSORS_READ_STATUS_NORMAL;
enum sensors_read_status_e mag_read_status = SENSORS_READ_STATUS_NORMAL;

void sensors_imu_status_check()
{
	float acc[3];
	imu_acc_get(acc);
	float norm_acc=sqrtf(acc[0]*acc[0]+acc[1]*acc[1]+acc[2]*acc[2]);
	static uint32_t err_counter = 0;
	if(norm_acc < 0.01f){
		err_counter++;
	}else{
		err_counter=0;
	}
	
	if(err_counter>10){
		imu_read_status = SENSORS_READ_STATUS_FAIL;
	}else{
		imu_read_status = SENSORS_READ_STATUS_NORMAL;
	}

}

void sensors_mag_status_check()
{
	float mag[3];
	mag_value_get(mag);
	float norm_mag=sqrtf(mag[0]*mag[0]+mag[1]*mag[1]+mag[2]*mag[2]);
	static uint32_t err_counter_mag = 0;
	if(norm_mag < 0.01f){
		err_counter_mag++;
	}else{
		err_counter_mag=0;
	}
	
	if(err_counter_mag>10){
		mag_read_status = SENSORS_READ_STATUS_FAIL;
	}else{
		mag_read_status = SENSORS_READ_STATUS_NORMAL;
	}
}

void sensors_status_check()
{
	sensors_imu_status_check();
	sensors_mag_status_check();
}

enum sensors_read_status_e imu_read_status_get()
{
	return imu_read_status;
}

enum sensors_read_status_e mag_read_status_get()
{
	return mag_read_status;
}






