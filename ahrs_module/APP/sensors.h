#ifndef _SENSORS_H_
#define _SENSORS_H_

enum sensors_read_status_e{
    SENSORS_READ_STATUS_NORMAL = 0,
		SENSORS_READ_STATUS_FAIL,
};

void sensors_status_check(void);

enum sensors_read_status_e imu_read_status_get(void);
enum sensors_read_status_e mag_read_status_get(void);

#endif

