del /Q Obj\*.* /s /q /f
del /Q *.bak
del /Q *.dep
del /Q *.Obj
del /Q *.List
del /Q *.Administrator
del /Q *.pyc
del /Q *.bin
del /Q *.uvopt
del /Q *.sconsign.delite
del /Q *.gitignore
del /Q *.map
del /Q *.axf
del /Q tmpcmd.txt
del /Q JLinkLog.txt
del /Q *.o
del /Q *.d
del /Q *.crf
@echo off