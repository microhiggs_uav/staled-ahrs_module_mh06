
#include <stdio.h>
#include <string.h>

#include "I2C.h"
#include "icm20602.h"
#include "system.h"

typedef union{
	uint8_t    b[2];
	int16_t    w;
}int16_t_bytes_u;

int16_t int16_t_from_bytes(uint8_t bytes[])
{
	int16_t_bytes_u data;
	
	data.b[1] = bytes[0];
	data.b[0] = bytes[1];

	return data.w;
}

#define  I2C_ICM20602_ADD			0xd2

#define A_PWR_MGMT_1    0x6b 	
#define A_SMPLRT_DIV    0x19 	
#define A_CONFIG        0x1a 	
#define A_GYRO_CONFIG   0x1b 	
#define A_ACCEL_CONFIG  0x1c 	
#define A_FIFO_EN				0x23 	
#define A_INT_ENABLE    0x38 	
#define A_USER_CTRL			0x6a 	
#define ACCEL_STAR_ADD  0X3b 	
#define TEMP_STAR_ADD   0x41 	
#define GYRO_STAR_ADD   0x43 

#define ACC_SCALE    0.0011971008f				
#define GYRO_SCALE 	 2.663161*1e-4f

u8 ICM20602_buff[14] = {0};
float gyro_imu[3] = {0.0f};
float acc_imu[3] = {0.0f};

void Init_Icm20602()
{
	uint8_t check_icm = 1;
	do{
	check_icm =Write_MEMS_CMD(I2C_ICM20602_ADD, A_PWR_MGMT_1, 0x00);		// 0x03  z��
		Delay_Ms(100);
	}while(check_icm != 0);
	
	check_icm = 1;
	do{
		check_icm =Write_MEMS_CMD(I2C_ICM20602_ADD, A_CONFIG, 0x03);			// 41Hz;  0x04 - 20Hz
		Delay_Ms(100);
	}while(check_icm != 0);
	
	check_icm = 1;
	do{
		check_icm =Write_MEMS_CMD(I2C_ICM20602_ADD, A_GYRO_CONFIG, 0x08);		// 0x08);		// 1000:0x10; 2000:0x18;500:0x08	
		Delay_Ms(100);
	}while(check_icm != 0);
	
	check_icm = 1;
	do{
		check_icm =Write_MEMS_CMD(I2C_ICM20602_ADD, A_ACCEL_CONFIG, 0x08);	// 8g:0x10;4g:0x08;2g:0x00		
		Delay_Ms(100);
	}while(check_icm != 0);
	Delay_Ms(5);
	
	check_icm = 1;
	do{
		check_icm =Write_MEMS_CMD(I2C_ICM20602_ADD, A_FIFO_EN, 0x00);	
		Delay_Ms(100);
	}while(check_icm != 0);
	
	check_icm = 1;
	do{
		check_icm =Write_MEMS_CMD(I2C_ICM20602_ADD, A_INT_ENABLE, 0x00);
		Delay_Ms(100);
	}while(check_icm != 0);
	
	check_icm = 1;
	do{
		check_icm =Write_MEMS_CMD(I2C_ICM20602_ADD, A_USER_CTRL, 0x00);
		Delay_Ms(100);
	}while(check_icm != 0);
	
	check_icm = 1;
	do{
		check_icm = Write_MEMS_CMD(I2C_ICM20602_ADD, A_PWR_MGMT_1, 0x03);
		Delay_Ms(100);
	}while(check_icm != 0);
}

void Read_Icm20602_data(float value_gyro[3],float value_acc[3])
{
	I2C_SW_Get(I2C_ICM20602_ADD,ICM20602_buff, 0x3b,14);
	
	int16_t acc_raw[3], gyro_raw[3];
	acc_raw[0]	= (((int16_t)ICM20602_buff[0])  << 8) | ICM20602_buff[1];
	acc_raw[1]	= (((int16_t)ICM20602_buff[2])  << 8) | ICM20602_buff[3];
	acc_raw[2]	= (((int16_t)ICM20602_buff[4])  << 8) | ICM20602_buff[5];

	gyro_raw[0] = (((int16_t)ICM20602_buff[8])  << 8) | ICM20602_buff[9];
	gyro_raw[1] = (((int16_t)ICM20602_buff[10]) << 8) | ICM20602_buff[11];
	gyro_raw[2] = (((int16_t)ICM20602_buff[12]) << 8) | ICM20602_buff[13]; 
	
	value_acc[0] = (float)acc_raw[1]* ACC_SCALE;
	value_acc[1] = (float)acc_raw[0]* ACC_SCALE;
	value_acc[2] = (float)-acc_raw[2]* ACC_SCALE;
	
	value_gyro[0] = (float)gyro_raw[1]* GYRO_SCALE;
	value_gyro[1] = (float)gyro_raw[0]* GYRO_SCALE;
	value_gyro[2] = (float)-gyro_raw[2]* GYRO_SCALE;
	
	memcpy(gyro_imu, value_gyro, sizeof(gyro_imu));
	memcpy(acc_imu, value_acc, sizeof(acc_imu));
	
#if 0
	printf("mpu:%6.3f,%6.3f,%6.3f,%6.3f,%6.3f,%6.3f\r\n",
					value_acc[0],value_acc[1],value_acc[2],value_gyro[0],value_gyro[1],value_gyro[2]);
#endif

}


void imu_gyro_get(float gyro[3])
{
	memcpy(gyro, gyro_imu, sizeof(gyro_imu));
}

void imu_acc_get(float acc[3])
{
	memcpy(acc, acc_imu, sizeof(acc_imu));
}




