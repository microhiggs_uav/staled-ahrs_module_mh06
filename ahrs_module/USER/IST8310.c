
#include <string.h>
#include <stdio.h>
#include "IST8310.h"

#define SCALE_MAG		3.0f
float mag_raw[3] = {0.0f};

IST8310 Mag_IST8310;

# define CTRL3_SAMPLEAVG_16		0x24	/* Sample Averaging 16 */
# define CTRL3_SAMPLEAVG_8		0x1b	/* Sample Averaging 8 */
# define CTRL3_SAMPLEAVG_4		0x12	/* Sample Averaging 4 */
# define CTRL3_SAMPLEAVG_2		0x09	/* Sample Averaging 2 */

#define ADDR_CTRL4				0x42
# define CTRL4_SRPD				0xC0	/* Set Reset Pulse Duration */

#define IST8310_REG_AVERAGE 0x41

void IST8310_Init(void)
{
	memset(&Mag_IST8310,0x00,sizeof(Mag_IST8310));
	
	uint8_t check_ist = 1;
	do{
		check_ist = Write_MEMS_CMD(IST8310_SLAVE_ADDRESS,IST8310_REG_CNTRL1,0x01);//Single Measurement Mode
		Delay_Ms(100);
	}while(check_ist != 0);
	
	check_ist = 1;
	do{
		check_ist = Write_MEMS_CMD(IST8310_SLAVE_ADDRESS,0x41,0x24);//开启16x内部平均
		Delay_Ms(100);
	}while(check_ist != 0);
	
  check_ist = 1;
	do{
		check_ist = Write_MEMS_CMD(IST8310_SLAVE_ADDRESS,0x42,0xC0);//Set/Reset内部平均	
		Delay_Ms(100);
	}while(check_ist != 0);
	
	check_ist = 1;
	do{
		check_ist = Write_MEMS_CMD(IST8310_SLAVE_ADDRESS,IST8310_REG_CNTRL1,0x01);//Single Measurement Mode
		Delay_Ms(100);
	}while(check_ist != 0);
}


void Get_Mag_IST8310(float mag[3])
{
	I2C_SW_Get(IST8310_SLAVE_ADDRESS,&Mag_IST8310.Buf[0],0x03,6);
	
	Mag_IST8310.Mag_Data[0]=(((int16_t)Mag_IST8310.Buf[1])<<8)|(int16_t)Mag_IST8310.Buf[0];
	Mag_IST8310.Mag_Data[1]=(((int16_t)Mag_IST8310.Buf[3])<<8)|(int16_t)Mag_IST8310.Buf[2];
	Mag_IST8310.Mag_Data[2]=(((int16_t)Mag_IST8310.Buf[5])<<8)|(int16_t)Mag_IST8310.Buf[4];

	Write_MEMS_CMD(IST8310_SLAVE_ADDRESS,IST8310_REG_CNTRL1,0x01);//Single Measurement Mode

  Mag_IST8310.x = Mag_IST8310.Mag_Data[1];
  Mag_IST8310.y = -Mag_IST8310.Mag_Data[0];
  Mag_IST8310.z = -Mag_IST8310.Mag_Data[2];
	
	mag[0] = SCALE_MAG*Mag_IST8310.x;
	mag[1] = SCALE_MAG*Mag_IST8310.y;
	mag[2] = SCALE_MAG*Mag_IST8310.z;
	
	memcpy(mag_raw,mag,sizeof(mag_raw));
}

void mag_value_get(float mag[3])
{
	memcpy(mag, mag_raw, sizeof(mag_raw));
}
