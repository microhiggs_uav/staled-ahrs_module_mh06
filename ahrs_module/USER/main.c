

#include <stdio.h>
#include <math.h>
#include "system.h"
#include "UART.h"
#include "icm20602.h"
#include "IST8310.h"
#include "led.h"
#include "key.h"
#include "flash.h"
#include "sensors.h"

#include "calibrator.h"
#include "att_est.h"
//#include "att_est_so3.h"
//#include "MahonyAHRS.h"
#include "comm_link.h"


uint32_t previousTime = 0;
uint32_t cycleTime = 0;    

int main(void)
{ 
	Delay (0xffff);
	SysTick_Configuration();
	
//		Delay_Ms(200);
//	Delay_Ms(200);
//	Delay_Ms(200);
//	Delay_Ms(200);
//	Delay_Ms(200);
//	Delay_Ms(200);
//	Delay_Ms(200);
//	Delay_Ms(200);
	
	Delay_Ms(50);
	Init_All_Periph();
	Delay_Ms(5);
	
	calib_init();
	comm_link_init();
	Delay_Ms(50);
	
	clear_sysTick();
	previousTime = systick_count;
  while (1){
		static uint32_t rate_time  = 0;
//		if((int16_t)(systick_count-rate_time) >0 ){ 
			rate_time = systick_count + 50;
			
			float value_gyro[3],value_acc[3],value_mag[3];
			Get_Mag_IST8310(value_mag);
			Read_Icm20602_data(value_gyro, value_acc);	
			float norm_mag_0=sqrtf(value_mag[0]*value_mag[0]+value_mag[1]*value_mag[1]+value_mag[2]*value_mag[2]);
//			printf("mag_0:%6.3f,%6.3f,%6.3f\r\n",value_mag[0],value_mag[1],value_mag[2]);
//		
			calib_update(value_gyro,value_acc,value_mag);
			float norm_mag_1=sqrtf(value_mag[0]*value_mag[0]+value_mag[1]*value_mag[1]+value_mag[2]*value_mag[2]);
//			printf("mag_1:%6.3f,%6.3f,%6.3f\r\n",value_mag[0],value_mag[1],value_mag[2]);
//			printf("mag:%6.3f	%6.3f\r\n",norm_mag_0,norm_mag_1);
			
			bool ahrs_inited = init_AHRS(value_gyro,value_acc,value_mag);
			if(ahrs_inited){			
				cycleTime = systick_count - previousTime;
				float dt = 0.0001f*cycleTime;
				if(dt>0.05f) dt=0.05f;
				update_AHRS(value_gyro,value_acc,value_mag,dt);
#if 1				
				static uint64_t counter_p = 0;
				counter_p++;
				if(counter_p%50==0){
					if(counter_p==6000){	
						mag_calib_start();
					}
					float angle_rpy[3];
					get_angle_rpy(angle_rpy);
//					float mx=value_mag[0]*cosf(angle_rpy[1])+value_mag[2]*sinf(angle_rpy[1]);
//					float my=value_mag[0]*sinf(angle_rpy[0])*sinf(angle_rpy[1])+value_mag[1]*cosf(angle_rpy[1])-value_mag[2]*sinf(angle_rpy[0])*cosf(angle_rpy[1]);

					float my=value_mag[2]*sinf(angle_rpy[0])-value_mag[1]*cosf(angle_rpy[0]);
					float mx=value_mag[0]*cosf(angle_rpy[1])+value_mag[1]*sinf(angle_rpy[1])*sinf(angle_rpy[0])+value_mag[2]*sinf(angle_rpy[1])*cosf(angle_rpy[0]);


					angle_rpy[0] = 57.3f*angle_rpy[0];
					angle_rpy[1] = 57.3f*angle_rpy[1];
					angle_rpy[2] = 57.3f*angle_rpy[2];
					float yaw_mag = 57.3f*atan2f(my,mx);
					
					float angle_integ[3];
					get_gyro_integral(angle_integ);
					angle_integ[0] = 57.3f*angle_integ[0];
					angle_integ[1] = 57.3f*angle_integ[1];
					angle_integ[2] = 57.3f*angle_integ[2];
//					printf("att:%6.3f,%6.3f,%6.3f		%6.3f,%6.3f,%6.3f\r\n",
//					angle_rpy[0],angle_rpy[1],angle_rpy[2],angle_integ[0],angle_integ[1],angle_integ[2]);
					
//					printf("att:%6.3f,%6.3f,%6.3f	%6.3f\r\n",angle_rpy[0],angle_rpy[1],angle_rpy[2],yaw_mag);
				}				
#endif				
				previousTime = systick_count;	
			}		
//		}
		
		static uint32_t low_rate_time  = 0;
		if((int16_t)(systick_count-low_rate_time) >0 ){ 
			low_rate_time = systick_count + 500;	//200
			sensors_status_check();
//			comm_link_update();
			led_upload();
		}
	}
}



