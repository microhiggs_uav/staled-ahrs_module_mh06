

#ifndef _MPU6050_H_
#define _MPU6050_H_

#include <stdint.h>

void Init_Icm20602(void);
void Read_Icm20602_data(float value_gyro[3],float value_acc[3]);


void imu_gyro_get(float gyro[3]);
void imu_acc_get(float acc[3]);

#endif






