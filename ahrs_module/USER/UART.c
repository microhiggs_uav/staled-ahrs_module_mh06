
#include <string.h>

#include "UART.h"
#include "system.h"

typedef struct __FILE FILE;

#define UART2_DR_Address        0x40004404
#define UART3_DR_Address        0x40004804

// uart2-to CP2102
// uart3-debug

// uart3 - debug
uint8_t Uart3_Rx_Data[UART3_BUFFER_LEN] = {0};   
volatile uint8_t Rx_Sem_Debug = 0;
volatile uint8_t Rx_In_Debug = 0;
volatile uint8_t Uart3_Txing_fig = 0;		//uart3发送标志	为1表示正在发送，为0表示发送成功

// uart2 - to CP2102
uint8_t Uart2_Rx_Data[UART2_REC_BUFFER_LEN] = {0};	
static uint8_t Uart2_Rx_temp[UART2_REC_BUFFER_LEN] = {0};
static uint8_t Uart2_Txing_fig = 0;			//uart2发送标志	
volatile uint8_t UART2_RX_Updata_fig =0;		//UART2更新标志 为0没有更新?   为1有更新，


int fputc(int ch,FILE *f)
{
	USART_SendData(USART2 ,(unsigned char)ch);
	while(USART_GetFlagStatus (USART2,USART_FLAG_TC)!=SET);
	return (ch);
}


void UART2_Configuration(u32 BaudRate)
{
		USART_InitTypeDef USART_InitStructure;
		GPIO_InitTypeDef  GPIO_InitStructure;
		NVIC_InitTypeDef NVIC_InitStructure;
	  DMA_InitTypeDef  DMA_InitStructure;
	
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA ,ENABLE);
	
	//TX PA2
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 ; 
	  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
		GPIO_Init(GPIOA, &GPIO_InitStructure);
	//RX PCA3
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3; 
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
		GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	 /* NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
    NVIC_Init(&NVIC_InitStructure);*/
		
		NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel6_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
		
	
		USART_InitStructure.USART_BaudRate = BaudRate;
		USART_InitStructure.USART_WordLength = USART_WordLength_8b;
		USART_InitStructure.USART_StopBits = USART_StopBits_1;
		USART_InitStructure.USART_Parity = USART_Parity_No;
		USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;	
		USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;		
		USART_Init(USART2, &USART_InitStructure);
		
		USART_Cmd(USART2,ENABLE);
		DMA_DeInit(DMA1_Channel6);
		DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)UART2_DR_Address;
		DMA_InitStructure.DMA_MemoryBaseAddr = (u32)Uart2_Rx_temp; 
		DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC; 
		DMA_InitStructure.DMA_BufferSize = UART2_REC_BUFFER_LEN;
		DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; 
		DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
		DMA_InitStructure.DMA_PeripheralDataSize = DMA_MemoryDataSize_Byte; 
		DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte; 
		DMA_InitStructure.DMA_Mode = DMA_Mode_Circular; // fixed
		DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;  // up to user
		DMA_InitStructure.DMA_M2M = DMA_M2M_Disable; // fixed
		
		DMA_Init(DMA1_Channel6, &DMA_InitStructure);
		DMA_ITConfig(DMA1_Channel6, DMA_IT_TC, ENABLE);
		
		USART_DMACmd(USART2, USART_DMAReq_Rx , ENABLE);
		DMA_Cmd(DMA1_Channel6, ENABLE);       
		
		// USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
}


void USART3_Configuration(u32 BaudRate)
{
		USART_InitTypeDef USART_InitStructure;
		GPIO_InitTypeDef  GPIO_InitStructure;
		NVIC_InitTypeDef NVIC_InitStructure;
	
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE);
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC,ENABLE);
	
		//TX PB10
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10 ; 
//		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
		GPIO_Init(GPIOB, &GPIO_InitStructure);
		//RX PB11
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11; 
//  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
//		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
		GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	  NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
	
	
		USART_InitStructure.USART_BaudRate = BaudRate;
		USART_InitStructure.USART_WordLength = USART_WordLength_8b;
		USART_InitStructure.USART_StopBits = USART_StopBits_1;
		USART_InitStructure.USART_Parity = USART_Parity_No;
		USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	
		USART_InitStructure.USART_Mode = USART_Mode_Tx;// | USART_Mode_Rx;	仅发送使能
		USART_Init(USART3, &USART_InitStructure);
		USART_Cmd(USART3,ENABLE);
		USART_ITConfig(USART3, USART_IT_RXNE, DISABLE);//ENABLE);
}

void Uart3_Dma_Send(u8* pBuffer, u16 NumByteToWrite)
{
		DMA_InitTypeDef  DMA_InitStructure;
		NVIC_InitTypeDef NVIC_InitStructure;
	
	  Uart3_Txing_fig = 1;
		DMA_DeInit(DMA1_Channel2);
		DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)UART3_DR_Address;
	
		DMA_InitStructure.DMA_MemoryBaseAddr = (u32)pBuffer; 
	
		DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST; 
		DMA_InitStructure.DMA_BufferSize = NumByteToWrite;
		DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; // fixed
		DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable; // fixed
		DMA_InitStructure.DMA_PeripheralDataSize = DMA_MemoryDataSize_Byte; //fixed
		DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte; //fixed
		DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;// fixed
		DMA_InitStructure.DMA_Priority = DMA_Priority_High;  // up to user
		DMA_InitStructure.DMA_M2M = DMA_M2M_Disable; // fixed
		
		DMA_Init(DMA1_Channel2, &DMA_InitStructure);
		DMA_ITConfig(DMA1_Channel2, DMA_IT_TC, ENABLE);
		
		NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel2_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
		
		USART_DMACmd(USART3, USART_DMAReq_Tx, ENABLE);
    DMA_Cmd(DMA1_Channel2, ENABLE);
}


void Uart2_Dma_Send(const u8* pBuffer, u16 NumByteToWrite)
{
		DMA_InitTypeDef  DMA_InitStructure;
		NVIC_InitTypeDef NVIC_InitStructure;
	
	  Uart2_Txing_fig = 1;
		DMA_DeInit(DMA1_Channel7);
		DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)UART2_DR_Address;
		
		DMA_InitStructure.DMA_MemoryBaseAddr = (u32)pBuffer; 
		DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST; 
		DMA_InitStructure.DMA_BufferSize = NumByteToWrite;
		DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; // fixed
		DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable; // fixed
		DMA_InitStructure.DMA_PeripheralDataSize = DMA_MemoryDataSize_Byte; //fixed
		DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte; //fixed
		DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;// fixed
		DMA_InitStructure.DMA_Priority = DMA_Priority_High;  // up to user
		DMA_InitStructure.DMA_M2M = DMA_M2M_Disable; // fixed
		
		DMA_Init(DMA1_Channel7, &DMA_InitStructure);
		DMA_ITConfig(DMA1_Channel7, DMA_IT_TC, ENABLE);
		
		NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel7_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
		
		USART_DMACmd(USART2, USART_DMAReq_Tx, ENABLE);
    DMA_Cmd(DMA1_Channel7, ENABLE);
}

void Uart2_dma_receive_isr(void)
{
	if (DMA_GetFlagStatus(DMA1_FLAG_TC6))
  	{
#if 1
	  for(u8 i=0;i<UART2_REC_BUFFER_LEN;i++)
		{
			Uart2_Rx_Data[i]=Uart2_Rx_temp[i];
		}
#endif
		// memcpy(Uart2_Rx_Data,Uart2_Rx_temp,UART2_REC_BUFFER_LEN);

	   	UART2_RX_Updata_fig =1;
	    DMA_ClearFlag(DMA1_FLAG_TC6);
  	}
  if (DMA_GetFlagStatus(DMA1_FLAG_GL6))
  {
    DMA_ClearFlag( DMA1_FLAG_GL6);
  }
    if (DMA_GetFlagStatus(DMA1_FLAG_HT6))
  {
    DMA_ClearFlag( DMA1_FLAG_HT6);
  }
  
  	if (DMA_GetFlagStatus(DMA1_FLAG_TE6))
  {
    DMA_ClearFlag( DMA1_FLAG_TE6);
  }
}


void USART2_IRQHandler(void)  
{
	if(USART_GetFlagStatus(USART2, USART_FLAG_ORE) != RESET) 
	{ 
		USART_ClearFlag(USART2, USART_FLAG_ORE); //清除溢出中断
	}
	if(USART_GetFlagStatus(USART2, USART_FLAG_NE) != RESET) 
	{ 
		USART_ClearFlag(USART2, USART_FLAG_NE); //噪音错误中断
	}
	if(USART_GetFlagStatus(USART2, USART_FLAG_FE) != RESET) 
	{ 
		USART_ClearFlag(USART2, USART_FLAG_FE); //帧错误中断
	}
	if(USART_GetITStatus(USART2,USART_IT_RXNE) != RESET)
	{
		// 
	}
}	


void USART3_IRQHandler(void)  
{
	if(USART_GetFlagStatus(USART3, USART_FLAG_ORE) != RESET) 
	{ 
		USART_ClearFlag(USART3, USART_FLAG_ORE); //清除溢出中断
	}
	if(USART_GetFlagStatus(USART3, USART_FLAG_NE) != RESET) 
	{ 
		USART_ClearFlag(USART3, USART_FLAG_NE); //噪音错误中断
	}
	if(USART_GetFlagStatus(USART3, USART_FLAG_FE) != RESET) 
	{ 
		USART_ClearFlag(USART3, USART_FLAG_FE); //帧错误中断
	}
	if(USART_GetITStatus(USART3,USART_IT_RXNE) != RESET)
	{
		// 
	}
}


void Uart2_dma_send_isr(void)
{
	if (DMA_GetFlagStatus(DMA1_FLAG_TC7))
  {        
    USART_DMACmd(USART2, USART_DMAReq_Tx, DISABLE);
    DMA_Cmd(DMA1_Channel7, DISABLE);   
    DMA_ClearFlag(DMA1_FLAG_TC7);
		Uart2_Txing_fig = 0;
  }
  if (DMA_GetFlagStatus(DMA1_FLAG_GL7))
  {
    DMA_ClearFlag( DMA1_FLAG_GL7);
  }
    if (DMA_GetFlagStatus(DMA1_FLAG_HT7))
  {
    DMA_ClearFlag( DMA1_FLAG_HT7);
  }
	if (DMA_GetFlagStatus(DMA1_FLAG_TE7))
  {
    DMA_ClearFlag( DMA1_FLAG_TE7);
  }
}

void Uart3_dma_send_isr(void)
{
	if (DMA_GetFlagStatus(DMA1_FLAG_TC2))
  {   
			static uint32_t cc = 0;
		cc++;
    USART_DMACmd(USART3, USART_DMAReq_Tx, DISABLE);
    DMA_Cmd(DMA1_Channel2, DISABLE);   
    DMA_ClearFlag(DMA1_FLAG_TC2);
		Uart3_Txing_fig = 0;
  }
  if (DMA_GetFlagStatus(DMA1_FLAG_GL2))
  {
    DMA_ClearFlag( DMA1_FLAG_GL2);
  }
    if (DMA_GetFlagStatus(DMA1_FLAG_HT2))
  {
    DMA_ClearFlag( DMA1_FLAG_HT2);
  }
	if (DMA_GetFlagStatus(DMA1_FLAG_TE2))
  {
    DMA_ClearFlag( DMA1_FLAG_TE2);
  }
}

void DMA1_Channel6_IRQHandler(void)
{
  Uart2_dma_receive_isr();
}

void DMA1_Channel7_IRQHandler(void)
{
	Uart2_dma_send_isr();
}


void DMA1_Channel2_IRQHandler(void)
{
	Uart3_dma_send_isr();
}







