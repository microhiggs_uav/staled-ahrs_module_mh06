#ifndef _KEY_H_

#define _KEY_H_

#include "system.h"

#define KEY_PRES_UP	     0       
#define KEY_PRES_DOWN    1      

// PA8 - button
#define KEY_STATE    GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0)

void Key_Init(void);

int Key_Scan(int mode);

#endif
