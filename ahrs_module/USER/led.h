

#ifndef __LED_H__
#define __LED_H__

#include <stdbool.h>
#include "system.h"

#if 1
// MH LED - PC13-green/PC14-blue

// Green
#define LED1_ON  	  GPIO_ResetBits(GPIOC, GPIO_Pin_13)
#define LED1_OFF     	GPIO_SetBits(GPIOC, GPIO_Pin_13)			

// Blue
#define LED2_ON   		GPIO_ResetBits(GPIOC, GPIO_Pin_14)
#define LED2_OFF  			GPIO_SetBits(GPIOC, GPIO_Pin_14)



void led_init(void);
void led_upload(void);

#endif

#endif





