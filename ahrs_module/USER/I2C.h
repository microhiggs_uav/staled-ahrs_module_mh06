#ifndef __i2c_h__
#define __i2c_h__


#include "system.h"
#include <stdbool.h>


uint8_t i2c_sw_init(void);

uint8_t Read_MEMS_byete(uint8_t u8Address, uint8_t add);
uint8_t Write_MEMS_CMD(uint8_t u8Address, uint8_t add, uint8_t cmd);
int32_t I2C_SW_Get(uint8_t u8Address, uint8_t* p8Data,uint8_t add, uint32_t u32ByteSize);
int32_t I2C_SW_Send(uint8_t u8Address, uint8_t* p8Data, uint8_t add, uint32_t u32ByteSize);


#endif
