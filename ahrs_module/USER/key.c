
#include "key.h"

void Key_Init(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
  
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    // PA8
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;  
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;		// up
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_ResetBits(GPIOA,GPIO_Pin_8);
}

int Key_Scan(int mode)
{     
    static int key_up=1;
    if(mode)	key_up=1; 
		
    if(key_up && KEY_STATE==1){
        Delay_Ms(10);
        key_up=0;
        if(KEY_STATE==1){
					return KEY_PRES_DOWN;
				}
		}else if(KEY_STATE==0){
				key_up=1;
		}
		return KEY_PRES_UP;
}
