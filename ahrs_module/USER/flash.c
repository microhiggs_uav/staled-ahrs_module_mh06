
#include <stdint.h>
#include <stdio.h>
#include "flash.h"

#define FLASH_SIZE		128
#define SECTOR_SIZE		1024

enum flash_status_e flash_work_status = FLASH_STATUS_NORMAL;

uint16_t FLASH_ReadHalfWord(uint32_t address)
{
	return*(__IO uint16_t*)address;
}

int32_t FLASH_ReadWord(uint32_t address)
{
	uint16_t temp1,temp2;
	temp1=*(__IO uint16_t*)address;
	temp2=*(__IO uint16_t*)(address+2);

	if(temp1==0xffff && temp2==0xffff){
		return 0;
	}else{
		return(temp2<<16)+temp1;
	}
}

void FLASH_ReadData(uint32_t startAddress,int32_t *readData,uint16_t countToRead)
{
	uint16_t dataIndex;
	for(dataIndex=0;dataIndex<countToRead;dataIndex++){
		readData[dataIndex]=FLASH_ReadWord(startAddress+dataIndex*4);
	}
}

void FLASH_WriteData(uint32_t startAddress,int32_t *writeData,uint16_t countToWrite)
{
	if(startAddress<FLASH_BASE||((startAddress+countToWrite*4)>=(FLASH_BASE+1024*FLASH_SIZE))){
		return;
	}

#if 1	
	FLASH_Unlock();
	uint32_t offsetAddress=startAddress-FLASH_BASE;
	uint32_t sectorPosition=offsetAddress/SECTOR_SIZE;

	uint32_t sectorStartAddress=sectorPosition*SECTOR_SIZE+FLASH_BASE;
	 
	FLASH_Status FLASHStatus = FLASH_ErasePage(sectorStartAddress);
	if(FLASHStatus==FLASH_COMPLETE){
		uint16_t dataIndex;
		for(dataIndex=0;dataIndex<countToWrite;dataIndex++){
			FLASH_ProgramWord(startAddress+dataIndex*4,writeData[dataIndex]);
		}
	}else{
		flash_work_status = FLASH_STATUS_WRITE_FAIL;
		printf("[flash]erase flsh failed\r\n");
	}

	FLASH_Lock();
#endif


#if 0	
	FLASH_UnlockBank1();
	FLASH_ClearFlag(FLASH_FLAG_EOP|FLASH_FLAG_PGERR|FLASH_FLAG_WRPRTERR);
	FLASH_Status FLASHStatus =FLASH_ErasePage(startAddress);
	if(FLASHStatus==FLASH_COMPLETE){
		FLASHStatus = FLASH_ProgramHalfWord(startAddress+WriteAddress,writeData);
	}else{
		printf("[flash]erase flsh failed\r\n");
	}
	FLASH_LockBank1();
#endif
}

enum flash_status_e FLASH_Status_get()
{
	return flash_work_status;
}


