
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include <stdint.h>

#include "I2C.h"
#include "UART.h"
#include "icm20602.h"
#include "IST8310.h"
#include "key.h"
#include "led.h"
#include "system.h"


void Delay(uint32_t nCount)
{
  for(; nCount != 0; nCount--);
}

void SysTick_Configuration(void)
{
  if(SysTick_Config(SystemCoreClock/10000))
	{
		while(1);
	}	
}


void RCC_Configuration(void)
{
	
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB , ENABLE);
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC , ENABLE);
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA, ENABLE);
//	RCC_APB1PeriphClockCmd( RCC_APB1Periph_I2C2, ENABLE); 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	RCC_AHBPeriphClockCmd( RCC_AHBPeriph_DMA1,ENABLE); 
	RCC_AHBPeriphClockCmd( RCC_AHBPeriph_DMA2,ENABLE); 
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE);
  
}


uint32_t Read_ID()
{
	uint32_t DevID[3];
	DevID[0] = *(vu32*)(0x1ffff7e8); 
  DevID[1] = *(vu32*)(0x1ffff7ec); 
  DevID[2] = *(vu32*)(0x1ffff7f0);
	return DevID[0]+DevID[1]+DevID[2];
}


uint32_t ID_mcu = 0xFFFFFFFF;
void Init_All_Periph(void)
{
	RCC_Configuration();	
	Delay_Ms(20);
	i2c_sw_init();
	Delay_Ms(20);
	
	//	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);	
	USART3_Configuration(115200);
//	UART2_Configuration(115200);
	UART2_Configuration(921600);
	Delay_Ms(250);
	
	Init_Icm20602();
	Delay_Ms(5);
	
	IST8310_Init();
	Delay_Ms(5);
	
//	Key_Init();
//	
	led_init();
	Delay_Ms(3);

	ID_mcu = Read_ID();
	Delay_Ms(2);
}

volatile uint32_t systick_count = 0;
void SysTick_Handler(void)
{
	systick_count++;
}
						    
void Delay_Ms(u8 nms)
{	 		  	  
	uint32_t tick_count = systick_count; 
	while(systick_count-tick_count < 10*(uint32_t)nms);
}  


void clear_sysTick()
{
	systick_count = 0;
}

void reset_sf(void) 
{ 
	__set_FAULTMASK(1);      	// 关闭所有中断
	NVIC_SystemReset();				// 复位
}


  								   






