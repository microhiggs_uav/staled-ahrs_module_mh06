#ifndef _FLASH_H_
#define _FLASH_H_

#include "system.h"   

enum flash_status_e{
    FLASH_STATUS_NORMAL = 0,
		FLASH_STATUS_READ_FAIL,
		FLASH_STATUS_WRITE_FAIL,
};

void FLASH_ReadData(uint32_t startAddress,int32_t *readData,uint16_t countToRead);
void FLASH_WriteData(uint32_t startAddress,int32_t *writeData,uint16_t countToWrite);

enum flash_status_e FLASH_Status_get(void);

#endif
