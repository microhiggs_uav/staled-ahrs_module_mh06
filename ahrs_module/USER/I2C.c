
#include "stm32f10x_exti.h"
#include "I2C.h"
#include "system.h"

#include <math.h>
#include <stdio.h>


#define I2C_CLK_set  (GPIOB->ODR |= 1<<8)						// GPIO_SetBits(GPIOB, GPIO_Pin_10)
#define I2C_CLK_clr  (GPIOB->ODR &= ~(1<<8))					// GPIO_ResetBits(GPIOB, GPIO_Pin_10)
#define I2C_DAT_set  (GPIOB->ODR |= 1<<9)						// GPIO_SetBits(GPIOB, GPIO_Pin_11)
#define I2C_DAT_clr  (GPIOB->ODR &= ~(1<<9))					// GPIO_ResetBits(GPIOB, GPIO_Pin_11)
#define I2C_DAT_in   (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_9))

#define i2c_del_count  8		// 4


void I2C_SW_Delay(uint32_t count)
{
	while(count--)
	{
		__NOP();
	}
}

uint8_t i2c_sw_init(void)
{
	
		GPIO_InitTypeDef  GPIO_InitStructure; 
	  RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB , ENABLE);
		GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_8 | GPIO_Pin_9; 
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
		GPIO_Init(GPIOB, &GPIO_InitStructure);	  

    I2C_DAT_clr;
    I2C_CLK_clr;
	
		Delay_Ms(50);
//	
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE); 
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;     
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
////																   
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
		GPIO_Init(GPIOA, &GPIO_InitStructure);
////		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
////		GPIO_Init(GPIOA, &GPIO_InitStructure);	
////		GPIO_SetBits(GPIOA, GPIO_Pin_4|GPIO_Pin_6); 
GPIO_SetBits(GPIOA, GPIO_Pin_4);
		
    return 0;
}

/**
  * @brief Send a byte
  * @param u8Data    Data
  * @retval 0        Receive ACK
  * @retval 1        Receive NACK
  */
int32_t I2C_SW_Send_byte(uint8_t u8Data)
{
    uint32_t u32count;
    for(u32count=0; u32count<8; u32count++) {
			  if(u8Data &(0x01 << (7-u32count)))
				{
					I2C_DAT_set;
				}
				else
				{
					I2C_DAT_clr;
				}
       // I2C_SW_SDA = u8Data>>(7-u32count);
			
			   I2C_SW_Delay(i2c_del_count);

        I2C_CLK_set;
         I2C_SW_Delay(i2c_del_count);
        I2C_CLK_clr;
    }
    I2C_DAT_set;
    I2C_SW_Delay(i2c_del_count);
    I2C_CLK_set;
    I2C_SW_Delay(i2c_del_count);
    u32count = I2C_DAT_in;
    I2C_CLK_clr;

    return u32count;
}

/**
  * @brief Send data
  * @param u8Address    I2C slave address
  * @param p8Data        data address
  * @param u32ByteSize    data length
  * @return data length
  */
int32_t I2C_SW_Send(uint8_t u8Address, uint8_t* p8Data, uint8_t add, uint32_t u32ByteSize)
{
    uint32_t u32count = 0;
    if(u32ByteSize == 0)
        return 0;

    I2C_DAT_set;
    I2C_CLK_set;
    I2C_SW_Delay(i2c_del_count);
    I2C_DAT_clr;
    I2C_SW_Delay(i2c_del_count);
    I2C_CLK_clr;
    I2C_SW_Delay(i2c_del_count);

    if(I2C_SW_Send_byte(u8Address))
        goto I2C_SW_Stop_Send;
		
		if(I2C_SW_Send_byte(add))
        goto I2C_SW_Stop_Send;

    while(u32count<u32ByteSize) {
        if(I2C_SW_Send_byte(*(p8Data+u32count++)))
            goto I2C_SW_Stop_Send;
    }
I2C_SW_Stop_Send:
    I2C_DAT_clr;
    I2C_SW_Delay(i2c_del_count);
    I2C_CLK_set;
    I2C_SW_Delay(i2c_del_count);
    I2C_DAT_set;

    return u32count;
}

/**
  * @brief Read a byte
  * @param u32Ack    Configure to ACK or NACK
  * @return data
  */
uint8_t I2C_SW_Get_byte(uint32_t u32Ack)
{
    uint32_t u32count;
    uint8_t u8Data=0;


    for(u32count=0; u32count<8; u32count++) {
        I2C_SW_Delay(i2c_del_count);
        I2C_CLK_set;
        I2C_SW_Delay(i2c_del_count);
        u8Data |= I2C_DAT_in << (7-u32count);
        I2C_CLK_clr;
    }
		if(u32Ack)
		{
			I2C_DAT_set;
		}
		else
		{
			I2C_DAT_clr;
		}
    //I2C_SW_SDA = u32Ack;
    I2C_SW_Delay(i2c_del_count);
    I2C_CLK_set;
    u32count = I2C_DAT_in;
    I2C_SW_Delay(i2c_del_count);
    I2C_CLK_clr;
    I2C_DAT_set;

    return u8Data;
}

/**
  * @brief Read data
  * @param u8Address    I2C slave address
  * @param p8Data        data address
  * @param u32ByteSize    data length
  * @return data length
  */
int32_t I2C_SW_Get(uint8_t u8Address, uint8_t* p8Data,uint8_t add, uint32_t u32ByteSize)
{
    uint32_t u32count = 0;
    if(u32ByteSize == 0)
        return 0;

    I2C_DAT_set;
    I2C_CLK_set;
    I2C_SW_Delay(i2c_del_count);
    I2C_DAT_clr;
    I2C_SW_Delay(i2c_del_count);
    I2C_CLK_clr;
    I2C_SW_Delay(i2c_del_count);
		
		if(I2C_SW_Send_byte(u8Address))
        goto I2C_SW_Stop_Get;
		
		if(I2C_SW_Send_byte(add))
        goto I2C_SW_Stop_Get;
		
    I2C_DAT_set;
    I2C_CLK_set;
    I2C_SW_Delay(i2c_del_count);
    I2C_DAT_clr;
    I2C_SW_Delay(i2c_del_count);
    I2C_CLK_clr;
    I2C_SW_Delay(i2c_del_count);
    if(I2C_SW_Send_byte((u8Address)|1))
        goto I2C_SW_Stop_Get;

    while(u32count<(u32ByteSize-1)) {
        *(p8Data+u32count++) = I2C_SW_Get_byte(0);
    }
    *(p8Data+u32count++) = I2C_SW_Get_byte(1);
I2C_SW_Stop_Get:
    I2C_DAT_clr;
    I2C_SW_Delay(i2c_del_count);
    I2C_CLK_set;
    I2C_SW_Delay(i2c_del_count);
    I2C_DAT_set;

    return u32count;
}

uint8_t Write_MEMS_CMD(uint8_t u8Address, uint8_t add, uint8_t cmd)
{
	 uint32_t u32count = 0;

    I2C_DAT_set;
    I2C_CLK_set;
    I2C_SW_Delay(i2c_del_count);
    I2C_DAT_clr;
   I2C_SW_Delay(i2c_del_count);
    I2C_CLK_clr;
    I2C_SW_Delay(i2c_del_count);

    if(I2C_SW_Send_byte(u8Address))
		{
			  u32count++;
        goto I2C_SW_Stop_Send;			 
		}
		
		if(I2C_SW_Send_byte(add))
        goto I2C_SW_Stop_Send;

  
		if(I2C_SW_Send_byte(cmd))
				goto I2C_SW_Stop_Send;
I2C_SW_Stop_Send:
    I2C_DAT_clr;
    I2C_SW_Delay(i2c_del_count);
    I2C_CLK_set;
    I2C_SW_Delay(i2c_del_count);
    I2C_DAT_set;

    return u32count;
}
uint8_t Read_MEMS_byete(uint8_t u8Address, uint8_t add)
{
		uint8_t temp=0;
    I2C_DAT_set;
    I2C_CLK_set;
    I2C_SW_Delay(i2c_del_count);
    I2C_DAT_clr;
    I2C_SW_Delay(i2c_del_count);
    I2C_CLK_clr;
    I2C_SW_Delay(i2c_del_count);
		
		if(I2C_SW_Send_byte(u8Address))
        goto I2C_SW_Stop_Get;
		
		if(I2C_SW_Send_byte(add))
        goto I2C_SW_Stop_Get;
		
    I2C_DAT_set;
    I2C_CLK_set;
    I2C_SW_Delay(i2c_del_count);
    I2C_DAT_clr;
    I2C_SW_Delay(i2c_del_count);
    I2C_CLK_clr;
    I2C_SW_Delay(i2c_del_count);
    if(I2C_SW_Send_byte((u8Address)|1))
        goto I2C_SW_Stop_Get;

    
    temp = I2C_SW_Get_byte(1);
I2C_SW_Stop_Get:
    I2C_DAT_clr;
    I2C_SW_Delay(i2c_del_count);
    I2C_CLK_set;
    I2C_SW_Delay(i2c_del_count);
    I2C_DAT_set;

    return temp;
}




















