
#include <stdio.h>
#include <math.h>
#include "led.h"
#include "calibrator.h"
#include "icm20602.h"
#include "IST8310.h"
#include "flash.h"
#include "sensors.h"

static void led1_ctrl(bool open);
static void led2_ctrl(bool open);

void led_init(void)
{
		GPIO_InitTypeDef GPIO_InitStructure;
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE); 
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;   
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 														   
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
		GPIO_Init(GPIOC, &GPIO_InitStructure);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
		GPIO_Init(GPIOC, &GPIO_InitStructure);	
		GPIO_SetBits(GPIOC, GPIO_Pin_14|GPIO_Pin_13);	  
}

void led_upload(void)
{
	static uint64_t counter_led = 0;
	static uint64_t counter_prev_led = 0;
	counter_led++;
	
	static bool led1_on = true;
	static bool led2_on = true;
	struct calibrate_status_s sensors = calib_status_get();
	enum sensors_read_status_e imu_status = imu_read_status_get();
	if(CALIB_STATUS_DOING == sensors.gyro || imu_status!=SENSORS_READ_STATUS_NORMAL){
		if(counter_led-counter_prev_led>2){
			led1_ctrl(led1_on);
			led1_on = !led1_on;
			counter_prev_led = counter_led;
			return;
		}
	}else{
		LED1_ON;
	}
	
	enum sensors_read_status_e mag_status = mag_read_status_get();
	if(CALIB_STATUS_DOING == sensors.mag || mag_status!=SENSORS_READ_STATUS_NORMAL){
		if(counter_led-counter_prev_led>2){
			led2_ctrl(led2_on);
			led2_on = !led2_on;
			counter_prev_led = counter_led;
			return;
		}
	}else{
		LED2_ON;
	}
	
	enum flash_status_e flash_status = FLASH_Status_get();
	if(FLASH_STATUS_NORMAL != flash_status){
		if(counter_led-counter_prev_led>8){
			led1_ctrl(led1_on);
			led1_on = !led1_on;
			led2_ctrl(led1_on);
			counter_prev_led = counter_led;
			return;
		}
	}else{
		LED2_ON;
	}
}


static void led1_ctrl(bool open)
{
	if(open){
		LED1_ON;
	}else{
		LED1_OFF;
	}
}

static void led2_ctrl(bool open)
{
	if(open){
		LED2_ON;
	}else{
		LED2_OFF;
	}
}
