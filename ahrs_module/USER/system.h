#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#include <stdint.h>
#include "stm32f10x.h"

extern volatile uint32_t systick_count;
extern uint32_t ID_mcu;

void Delay(uint32_t nCount);

void SysTick_Configuration(void);
void RCC_Configuration(void);

uint32_t Read_ID(void);
	
void Init_All_Periph(void);
						    
void Delay_Ms(uint8_t nms);
void clear_sysTick(void);


void reset_sf(void);


#endif


