#ifndef __UART_H__
#define __UART_H__

#include <stdint.h>
#include "I2C.h"

#define UART3_BUFFER_LEN					30
#define UART2_BUFFER_LEN					60	
#define UART2_REC_BUFFER_LEN			8

extern uint8_t Uart3_Rx_Data[UART3_BUFFER_LEN];
extern volatile uint8_t Rx_Sem_Debug;

extern uint8_t Uart2_Rx_Data[UART2_REC_BUFFER_LEN];
extern volatile uint8_t UART2_RX_Updata_fig;
void Uart2_Dma_Send(const u8* pBuffer, u16 NumByteToWrite);

extern volatile uint8_t Uart3_Txing_fig;
void Uart3_Dma_Send(u8* pBuffer, u16 NumByteToWrite);
void UART3_SendOneByte(u8 dat);

void UART2_Configuration(u32 BaudRate);
void USART3_Configuration(uint32_t BaudRate);

void Uart2_dma_rec_send_test(void);


#endif



